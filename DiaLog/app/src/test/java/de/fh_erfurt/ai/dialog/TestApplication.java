package de.fh_erfurt.ai.dialog;

import android.app.Application;

/**
 * @author Alexander Zaak
 * @version 1.0
 * @since 18.02.18
 */
public class TestApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
    }
}
