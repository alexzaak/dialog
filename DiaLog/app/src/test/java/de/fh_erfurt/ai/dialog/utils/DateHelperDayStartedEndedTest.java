package de.fh_erfurt.ai.dialog.utils;

import org.exparity.hamcrest.date.DateMatchers;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by alex on 22.07.17.
 */

public class DateHelperDayStartedEndedTest {

    private static final int YEAR = 2017;
    private static final int MONTH = Calendar.JULY;
    private static final int DAY = 22;

    private static final Date TEST_DATE;

    private static final Date EXPECTED_DATE_STARTED;
    private static final Date EXPECTED_DATE_ENDED;

    private static final Date INVALID_DATE_NULL = null;

    static {
        Calendar calendar = Calendar.getInstance();
        calendar.set(YEAR, MONTH, DAY, 12, 45, 59);
        TEST_DATE = calendar.getTime();

        calendar.set(YEAR, MONTH, DAY, 0, 0, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        EXPECTED_DATE_STARTED = calendar.getTime();

        calendar.set(YEAR, MONTH, DAY, 23, 59, 59);
        calendar.set(Calendar.MILLISECOND, 59);
        EXPECTED_DATE_ENDED = calendar.getTime();
    }

    @Test
    public void testDayStarted() throws Exception {
        Date actualDate = DateHelper.dayStarted(TEST_DATE);

        assertThat(actualDate, DateMatchers.sameInstant(EXPECTED_DATE_STARTED));
    }

    @Test
    public void testDayEnded() throws Exception {
        Date actualDate = DateHelper.dayEnded(TEST_DATE);

        assertThat(actualDate, DateMatchers.sameInstant(EXPECTED_DATE_ENDED));
    }

    @Test
    public void testDayStartedInvalid() throws Exception {
        Date actualDate = DateHelper.dayEnded(INVALID_DATE_NULL);

        assertThat(actualDate, is(equalTo(null)));
    }

    @Test
    public void testDayEndedInvalid() throws Exception {
        Date actualDate = DateHelper.dayEnded(INVALID_DATE_NULL);

        assertThat(actualDate, is(equalTo(null)));
    }
}
