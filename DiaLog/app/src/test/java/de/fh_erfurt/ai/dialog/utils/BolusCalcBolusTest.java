package de.fh_erfurt.ai.dialog.utils;

import android.util.Log;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import de.fh_erfurt.ai.dialog.enums.BloodsugarUnit;
import de.fh_erfurt.ai.dialog.enums.CarbsUnit;
import de.fh_erfurt.ai.dialog.model.User;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * Created by max on 23.07.17.
 */

@RunWith(PowerMockRunner.class)
@PrepareForTest({DateHelper.class, Log.class})
public class BolusCalcBolusTest {

    private BolusCalc bolusCalc;

    private static final User TEST_USER = new User();

    private static final double TEST_BOLUS_FACTOR_MORNING = 2f;
    private static final double TEST_BOLUS_FACTOR_NOON = 2.2f;
    private static final double TEST_BOLUS_FACTOR_EVENING = 1.5f;

    private static final double TEST_TARGET_AREA_FROM = 4.5f;
    private static final double TEST_TARGET_AREA_TO = 8.5f;

    private static final double TEST_BLOOD_SUGAR_IN_TARGET_AREA = 6f;
    private static final double TEST_BLOOD_SUGAR_LOW = 3f;
    private static final double TEST_CARBS = 4f;

    private static final double EXPECTED_BOLUS_LOW = 0f;
    private static final double EXPECTED_BOLUS_IN_TARGET_AREA_M = 8f;
    private static final double EXPECTED_BOLUS_IN_TARGET_AREA_N = 8.8f;
    private static final double EXPECTED_BOLUS_IN_TARGET_AREA_E = 6f;

    @Before
    public void setUp() {
        TEST_USER.setBolusFactorMorning(TEST_BOLUS_FACTOR_MORNING);
        TEST_USER.setBolusFactorNoon(TEST_BOLUS_FACTOR_NOON);
        TEST_USER.setBolusFactorEvening(TEST_BOLUS_FACTOR_EVENING);

        TEST_USER.setTargetAreaFrom(TEST_TARGET_AREA_FROM);
        TEST_USER.setTargetAreaTo(TEST_TARGET_AREA_TO);
        TEST_USER.setBloodSugarUnit(BloodsugarUnit.MMOL_L.getId());
        TEST_USER.setCarbsUnit(CarbsUnit.CU.getId());

        mockStatic(DateHelper.class);
        mockStatic(Log.class);

        this.bolusCalc = new BolusCalc(TEST_USER);
    }

    @Test
    public void testCalcBolusMorning() {
        when(DateHelper.getDayTime()).thenReturn(-1);

        double bolus = this.bolusCalc.calcBolus(TEST_BLOOD_SUGAR_IN_TARGET_AREA, TEST_CARBS);
        assertThat(bolus, is(equalTo(EXPECTED_BOLUS_IN_TARGET_AREA_M)));

        bolus = this.bolusCalc.calcBolus(TEST_BLOOD_SUGAR_LOW, TEST_CARBS);
        assertThat(bolus, is(equalTo(EXPECTED_BOLUS_LOW)));
    }

    @Test
    public void testCalcBolusNoon() {
        when(DateHelper.getDayTime()).thenReturn(0);

        double bolus = this.bolusCalc.calcBolus(TEST_BLOOD_SUGAR_IN_TARGET_AREA, TEST_CARBS);
        assertThat(bolus, is(equalTo(EXPECTED_BOLUS_IN_TARGET_AREA_N)));

        bolus = this.bolusCalc.calcBolus(TEST_BLOOD_SUGAR_LOW, TEST_CARBS);
        assertThat(bolus, is(equalTo(EXPECTED_BOLUS_LOW)));
    }

    @Test
    public void testCalcBolusEvening() {
        when(DateHelper.getDayTime()).thenReturn(1);

        double bolus = this.bolusCalc.calcBolus(TEST_BLOOD_SUGAR_IN_TARGET_AREA, TEST_CARBS);
        assertThat(bolus, is(equalTo(EXPECTED_BOLUS_IN_TARGET_AREA_E)));

        bolus = this.bolusCalc.calcBolus(TEST_BLOOD_SUGAR_LOW, TEST_CARBS);
        assertThat(bolus, is(equalTo(EXPECTED_BOLUS_LOW)));
    }
}
