package de.fh_erfurt.ai.dialog.controller;

import android.os.Build;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.rule.PowerMockRule;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import de.fh_erfurt.ai.dialog.BuildConfig;
import de.fh_erfurt.ai.dialog.TestApplication;
import de.fh_erfurt.ai.dialog.enums.BloodsugarUnit;
import de.fh_erfurt.ai.dialog.enums.CarbsUnit;
import de.fh_erfurt.ai.dialog.enums.Column;
import de.fh_erfurt.ai.dialog.model.LogEntry;
import de.fh_erfurt.ai.dialog.utils.DateHelper;
import de.fh_erfurt.ai.dialog.utils.DiaConverter;
import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.log.RealmLog;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * Created by max on 23.07.17.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = Build.VERSION_CODES.LOLLIPOP, application = TestApplication.class)
@PowerMockIgnore({"org.mockito.*", "org.robolectric.*", "android.*"})
@SuppressStaticInitializationFor("io.realm.internal.Util")
@PrepareForTest({UUID.class, Realm.class, RealmLog.class, LogManager.class, RealmObject.class, RealmResults.class, DateHelper.class, UserSettingsManager.class, DiaConverter.class})
public class LogManagerTest {

    @Rule
    public PowerMockRule rule = new PowerMockRule();

    private Realm mockRealm;
    private RealmQuery mockRealmQuery;

    private static final String TEST_ENTRY_ID = "fe7f0ab5-65eb-48d7-9b2e-c3d2ef71fe3b";
    private static final LogEntry TEST_ENTRY = new LogEntry();

    @Before
    public void setUp() throws Exception {

        mockStatic(RealmLog.class);
        mockStatic(Realm.class);
        mockStatic(UUID.class);
        mockStatic(RealmObject.class);
        mockStatic(DateHelper.class);
        mockStatic(UserSettingsManager.class);
        mockStatic(DiaConverter.class);

        Realm mockRealm = PowerMockito.mock(Realm.class);
        when(Realm.getDefaultInstance()).thenReturn(mockRealm);
        when(UUID.randomUUID().toString()).thenReturn(TEST_ENTRY_ID);

        this.mockRealm = mockRealm;
        this.mockRealmQuery = mock(RealmQuery.class);

        when(this.mockRealm.where(LogEntry.class)).thenReturn(this.mockRealmQuery);

        TEST_ENTRY.setBloodSugar(4d);
        TEST_ENTRY.setCarbs(2d);
        TEST_ENTRY.setCorrection(0d);
        TEST_ENTRY.setBolus(3d);
    }

    @Test
    public void testRealmInstance() throws Exception {
        assertThat(Realm.getDefaultInstance(), is(mockRealm));
    }

    @Test
    public void testGetLogEntry() {
        LogEntry mockEntry = mock(LogEntry.class);
        when(this.mockRealmQuery.equalTo("id", TEST_ENTRY_ID)).thenReturn(this.mockRealmQuery);
        when(this.mockRealmQuery.findFirst()).thenReturn(mockEntry);
        when(mockEntry.isValid()).thenReturn(true);

        LogManager.getLogEntry(TEST_ENTRY_ID);

        verify(mockRealm, times(1)).copyFromRealm(mockEntry);
        verify(mockRealm, times(1)).close();
    }

    @Test
    public void testGetAllLogEntries() {
        RealmResults<LogEntry> entries = mockRealmResults();
        when(this.mockRealmQuery.findAll()).thenReturn(entries);

        LogManager.getAllLogEntries();

        verify(mockRealm, times(1)).where(LogEntry.class);
        verify(mockRealmQuery, times(1)).findAll();
        verify(mockRealm, times(1)).close();
    }

    @Test
    public void testSaveLogEntry() {
        when(mockRealm.copyToRealmOrUpdate(TEST_ENTRY)).thenReturn(TEST_ENTRY);

        LogManager.saveLogEntry(TEST_ENTRY);

        verify(mockRealm, times(1)).beginTransaction();
        verify(mockRealm, times(1)).copyToRealmOrUpdate(TEST_ENTRY);
        verify(mockRealm, times(1)).commitTransaction();
        verify(mockRealm, times(1)).close();
    }

    @Test
    public void testDeleteLogEntry() {
        LogEntry mockEntry = mock(LogEntry.class);
        when(this.mockRealmQuery.equalTo("id", TEST_ENTRY_ID)).thenReturn(this.mockRealmQuery);
        when(this.mockRealmQuery.findFirst()).thenReturn(mockEntry);
        when(mockEntry.getId()).thenReturn(TEST_ENTRY_ID);

        LogManager.deleteLogEntry(mockEntry);

        verify(mockRealm, times(1)).beginTransaction();
        verify(mockRealm, times(1)).where(LogEntry.class);
        verify(mockRealmQuery, times(1)).equalTo("id", TEST_ENTRY_ID);
        verify(mockRealmQuery, times(1)).findFirst();
        verify(mockEntry, times(1)).deleteFromRealm();
        verify(mockRealm, times(1)).commitTransaction();
        verify(mockRealm, times(1)).close();
    }

    @Test
    public void testGetAveragedValue() {
        Date date1 = new Date();
        Date date2 = new Date();

        when(mockRealmQuery.between(Column.CREATED_AT.getName(), date1, date2)).thenReturn(mockRealmQuery);
        when(mockRealmQuery.average(anyString())).thenReturn(3d);

        double average = LogManager.getAveragedValue(Column.BLOODSUGAR, date1, date2);

        verify(mockRealm, times(1)).beginTransaction();
        verify(mockRealm, times(1)).where(LogEntry.class);
        verify(mockRealmQuery, times(1)).between(Column.CREATED_AT.getName(), date1, date2);
        verify(mockRealmQuery, times(1)).average(anyString());
        verify(mockRealm, times(1)).commitTransaction();
        verify(mockRealm, times(1)).close();

        assertThat(average, is(3d));
    }

    @Test
    public void testGetSummedValue() {
        Date date1 = new Date();
        Date date2 = new Date();

        when(mockRealmQuery.between(Column.CREATED_AT.getName(), date1, date2)).thenReturn(mockRealmQuery);
        when(mockRealmQuery.sum(anyString())).thenReturn(3d);

        double sum = LogManager.getSummedValue(Column.BLOODSUGAR, date1, date2);

        verify(mockRealm, times(1)).beginTransaction();
        verify(mockRealm, times(1)).where(LogEntry.class);
        verify(mockRealmQuery, times(1)).between(Column.CREATED_AT.getName(), date1, date2);
        verify(mockRealmQuery, times(1)).sum(anyString());
        verify(mockRealm, times(1)).commitTransaction();
        verify(mockRealm, times(1)).close();

        assertThat(sum, is(3d));
    }

    @Test
    public void testGetHyposCount() {
        Date date1 = new Date();
        Date date2 = new Date();

        when(DateHelper.dayStarted(any(Date.class))).thenReturn(date1);
        when(DateHelper.dayEnded(any(Date.class))).thenReturn(date2);

        when(mockRealmQuery.between(Column.CREATED_AT.getName(), date1, date2)).thenReturn(mockRealmQuery);
        when(mockRealmQuery.lessThan(anyString(), anyDouble())).thenReturn(mockRealmQuery);
        when(mockRealmQuery.count()).thenReturn(3L);
        when(UserSettingsManager.getThresholdHypo()).thenReturn(4d);

        long count = LogManager.getHyposCount();

        verify(mockRealm, times(1)).beginTransaction();
        verify(mockRealm, times(1)).where(LogEntry.class);
        verify(mockRealmQuery, times(1)).between(Column.CREATED_AT.getName(), date1, date2);
        verify(mockRealmQuery, times(1)).lessThan(anyString(), anyDouble());
        verify(mockRealmQuery, times(1)).count();
        verify(mockRealm, times(1)).commitTransaction();
        verify(mockRealm, times(1)).close();

        assertThat(count, is(3L));
    }

    @Test
    public void testGetHypersCount() {
        Date date1 = new Date();
        Date date2 = new Date();

        when(DateHelper.dayStarted(any(Date.class))).thenReturn(date1);
        when(DateHelper.dayEnded(any(Date.class))).thenReturn(date2);

        when(mockRealmQuery.between(Column.CREATED_AT.getName(), date1, date2)).thenReturn(mockRealmQuery);
        when(mockRealmQuery.greaterThan(anyString(), anyDouble())).thenReturn(mockRealmQuery);
        when(mockRealmQuery.count()).thenReturn(3L);
        when(UserSettingsManager.getThresholdHyper()).thenReturn(10d);

        long count = LogManager.getHypersCount();

        verify(mockRealm, times(1)).beginTransaction();
        verify(mockRealm, times(1)).where(LogEntry.class);
        verify(mockRealmQuery, times(1)).between(Column.CREATED_AT.getName(), date1, date2);
        verify(mockRealmQuery, times(1)).greaterThan(anyString(), anyDouble());
        verify(mockRealmQuery, times(1)).count();
        verify(mockRealm, times(1)).commitTransaction();
        verify(mockRealm, times(1)).close();

        assertThat(count, is(3L));
    }

    @Test
    public void testConvertLog() {
        RealmResults<LogEntry> entries = mockRealmResults();
        ArrayList<LogEntry> entryList = new ArrayList<>();
        entryList.add(TEST_ENTRY);
        when(this.mockRealmQuery.findAll()).thenReturn(entries);
        when(entries.iterator()).thenReturn(entryList.iterator());
        when(entries.size()).thenReturn(entryList.size());
        when(DiaConverter.convertBloodSugar(any(BloodsugarUnit.class), any(BloodsugarUnit.class), anyDouble())).thenReturn(3d);
        when(DiaConverter.convertCarbs(any(CarbsUnit.class), any(CarbsUnit.class), anyDouble())).thenReturn(3d);

        LogManager.convertLog(BloodsugarUnit.MMOL_L, BloodsugarUnit.MG_DL, CarbsUnit.CU, CarbsUnit.CU);

        verify(mockRealm, times(1)).beginTransaction();
        verify(mockRealm, times(1)).where(LogEntry.class);
        verify(mockRealmQuery, times(1)).findAll();
        verify(mockRealm, times(1)).copyToRealmOrUpdate(TEST_ENTRY);
        verify(mockRealm, times(1)).commitTransaction();
        verify(mockRealm, times(1)).close();

        assertThat(TEST_ENTRY.getBloodSugar(), is(3d));
        assertThat(TEST_ENTRY.getCarbs(), is(3d));
    }

    @SuppressWarnings("unchecked")
    private <T extends RealmObject> RealmResults<T> mockRealmResults() {
        return mock(RealmResults.class);
    }
}
