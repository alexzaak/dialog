package de.fh_erfurt.ai.dialog.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.test.mock.MockContext;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.robolectric.annotation.Config;

import java.io.File;
import java.io.IOException;

import de.fh_erfurt.ai.dialog.BuildConfig;
import de.fh_erfurt.ai.dialog.TestApplication;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by alex on 23.07.17.
 */

@RunWith(PowerMockRunner.class)
@Config(constants = BuildConfig.class, sdk = Build.VERSION_CODES.LOLLIPOP, application = TestApplication.class)
@PrepareForTest({Uri.class, PhotoManager.class, PermissionsCheck.class, Activity.class, Fragment.class, Intent.class, Environment.class, FileProvider.class})
public class PhotoManagerTest {

    private static final String[] PERMISSION_TAKE_PHOTO = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA};

    private static final String IMG_FILE_NAME = "img_221190.jpg";
    private static final String TEST_APP_NAME = "Dia:Log";
    private static final String TEST_PATH = "Pictures";

    private static final String TEST_PACKAGE_NAME = "de.fh_erfurt.ai.dialog";

    private static final String NULL_PATH = null;
    private static final String EMPTY_PATH = "";
    private static final String INVALID_PATH = "645#*de";

    private static final int CAMERA_REQUEST_CODE = 2211;
    private static final int CAMERA_REQUEST_CODE_INVALID = 5;

    private static final int RESULT_OK = Activity.RESULT_OK;
    private static final int RESULT_CANCELED = Activity.RESULT_CANCELED;

    private static final Activity ACTIVITY_INVALID_NULL = null;
    private static final Fragment FRAGMENT_INVALID_NULL = null;

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    private Activity mockedActivity;
    private Fragment mockedFragment;

    @Mock
    private MockContext mockedContext;
    private Intent mockedIntent = mock(Intent.class);

    @Before
    public void setUp() throws Exception {
        PowerMockito.mockStatic(Uri.class);
        PowerMockito.mockStatic(PermissionsCheck.class);
        PowerMockito.mockStatic(Intent.class);
        PowerMockito.mockStatic(Environment.class);
        PowerMockito.mockStatic(FileProvider.class);

        mockedActivity = PowerMockito.mock(Activity.class);
        mockedFragment = PowerMockito.mock(Fragment.class);

        when(mockedFragment.getContext()).thenReturn(mockedContext);
        when(mockedContext.getString(anyInt())).thenReturn(TEST_APP_NAME);
        when(mockedContext.getPackageName()).thenReturn(TEST_PACKAGE_NAME);
    }

    @Test
    public void testStartCameraPermissionsRequired() throws Exception {
        when(PermissionsCheck.checkRequiredPermissions(any(Activity.class), any(String[].class))).thenReturn(PERMISSION_TAKE_PHOTO);

        PhotoManager.startCamera(mockedActivity, mockedFragment);

        PowerMockito.verifyStatic(Mockito.times(1)); // Verify that the following mock method was called exactly 1 time
        PermissionsCheck.requestPermissions(mockedActivity, PERMISSION_TAKE_PHOTO);
    }

    @Test
    public void testStartCameraPermissionsGrantedDirExists() throws Exception {
        when(PermissionsCheck.checkRequiredPermissions(any(Activity.class), any(String[].class))).thenReturn(new String[]{});
        PowerMockito.whenNew(Intent.class).withAnyArguments().thenReturn(mockedIntent);
        //Set up a mocked File-object
        File mockedFile = Mockito.mock(File.class);
        Uri mockedUri = mock(Uri.class);

        Mockito.when(mockedFile.exists()).thenReturn(true);

        File tempFolder = folder.newFolder(TEST_PATH);
        when(Environment.getExternalStoragePublicDirectory(anyString())).thenReturn(tempFolder);

        //Trap constructor calls to return the mocked File-object
        PowerMockito.whenNew(File.class).withAnyArguments().thenReturn(mockedFile);

        when(FileProvider.getUriForFile(mockedContext, TEST_PACKAGE_NAME + "." + TEST_PACKAGE_NAME, mockedFile)).thenReturn(mockedUri);

        PhotoManager.startCamera(mockedActivity, mockedFragment);

        verify(mockedFragment, times(1)).startActivityForResult(mockedIntent, CAMERA_REQUEST_CODE);
    }

    @Test
    public void testStartCameraPermissionsGrantedDirNotExists() throws Exception {
        when(PermissionsCheck.checkRequiredPermissions(any(Activity.class), any(String[].class))).thenReturn(new String[]{});
        PowerMockito.whenNew(Intent.class).withAnyArguments().thenReturn(mockedIntent);
        //Set up a mocked File-object
        File mockedFile = Mockito.mock(File.class);
        Uri mockedUri = mock(Uri.class);

        Mockito.when(mockedFile.exists()).thenReturn(false);
        Mockito.when(mockedFile.mkdirs()).thenReturn(true);

        File tempFolder = folder.newFolder(TEST_PATH);
        when(Environment.getExternalStoragePublicDirectory(anyString())).thenReturn(tempFolder);

        //Trap constructor calls to return the mocked File-object
        PowerMockito.whenNew(File.class).withAnyArguments().thenReturn(mockedFile);

        when(FileProvider.getUriForFile(mockedContext, TEST_PACKAGE_NAME + "." + TEST_PACKAGE_NAME, mockedFile)).thenReturn(mockedUri);

        PhotoManager.startCamera(mockedActivity, mockedFragment);

        verify(mockedFragment, times(1)).startActivityForResult(mockedIntent, CAMERA_REQUEST_CODE);
    }

    @Test(expected = IOException.class)
    public void testStartCameraPermissionsGrantedDirNotCreated() throws Exception {
        when(PermissionsCheck.checkRequiredPermissions(any(Activity.class), any(String[].class))).thenReturn(new String[]{});
        PowerMockito.whenNew(Intent.class).withAnyArguments().thenReturn(mockedIntent);
        //Set up a mocked File-object
        File mockedFile = Mockito.mock(File.class);

        Mockito.when(mockedFile.exists()).thenReturn(false);
        Mockito.when(mockedFile.mkdirs()).thenReturn(false);

        File tempFolder = folder.newFolder(TEST_PATH);
        when(Environment.getExternalStoragePublicDirectory(anyString())).thenReturn(tempFolder);

        //Trap constructor calls to return the mocked File-object
        PowerMockito.whenNew(File.class).withAnyArguments().thenReturn(mockedFile);

        PhotoManager.startCamera(mockedActivity, mockedFragment);
    }

    @Test
    public void testStartCameraActivityNull() throws Exception {

        PhotoManager.startCamera(ACTIVITY_INVALID_NULL, mockedFragment);

        verify(mockedFragment, times(0)).getContext();
        PowerMockito.verifyStatic(Mockito.times(0)); // Verify that the following mock method was called exactly 1 time
        PermissionsCheck.checkRequiredPermissions(ACTIVITY_INVALID_NULL, PERMISSION_TAKE_PHOTO);
    }

    @Test
    public void testStartCameraFragmentNull() throws Exception {

        PhotoManager.startCamera(mockedActivity, FRAGMENT_INVALID_NULL);

        verify(mockedFragment, times(0)).getContext();
        PowerMockito.verifyStatic(Mockito.times(0)); // Verify that the following mock method was called exactly 1 time
        PermissionsCheck.checkRequiredPermissions(mockedActivity, PERMISSION_TAKE_PHOTO);
    }

    @Test
    public void testGetPhotoOnActivityResult() throws Exception {
        Uri uri = PowerMockito.mock(Uri.class);
        Whitebox.setInternalState(PhotoManager.class, "takenPhoto", uri);

        Uri photoUri = PhotoManager.getPhoto(CAMERA_REQUEST_CODE, RESULT_OK);
        assertThat(photoUri, is(notNullValue()));
        assertThat(photoUri, is(equalTo(uri)));
    }

    @Test
    public void testGetPhotoOnActivityResultInvalidRequestCode() throws Exception {
        Uri uri = PowerMockito.mock(Uri.class);
        Whitebox.setInternalState(PhotoManager.class, "takenPhoto", uri);

        Uri photoUri = PhotoManager.getPhoto(CAMERA_REQUEST_CODE_INVALID, RESULT_OK);
        assertThat(photoUri, is(nullValue()));
    }

    @Test
    public void testGetPhotoOnActivityResultInvalidResultCanceled() throws Exception {
        Uri uri = PowerMockito.mock(Uri.class);
        Whitebox.setInternalState(PhotoManager.class, "takenPhoto", uri);

        Uri photoUri = PhotoManager.getPhoto(CAMERA_REQUEST_CODE, RESULT_CANCELED);
        assertThat(photoUri, is(nullValue()));
    }

    @Test
    public void testGetPhotoOnActivityResultUriNull() throws Exception {
        Uri uri = null;
        Whitebox.setInternalState(PhotoManager.class, "takenPhoto", uri);

        Uri photoUri = PhotoManager.getPhoto(CAMERA_REQUEST_CODE, RESULT_OK);
        assertThat(photoUri, is(nullValue()));
    }

    @Test
    public void testGetPhotoByPath() throws Exception {
        Uri uri = PowerMockito.mock(Uri.class);
        PowerMockito.when(Uri.parse(anyString())).thenReturn(uri);

        File tempImg = folder.newFile(IMG_FILE_NAME);

        Uri photoUri = PhotoManager.getPhoto(tempImg.getPath());

        assertThat(photoUri, is(notNullValue()));
    }

    @Test
    public void testGetPhotoByInvalidPath() throws Exception {
        Uri uri = PowerMockito.mock(Uri.class);
        PowerMockito.when(Uri.parse(anyString())).thenReturn(uri);

        Uri photoUriNullPath = PhotoManager.getPhoto(NULL_PATH);
        assertThat(photoUriNullPath, is(Uri.EMPTY));


        Uri photoUriEmptyPath = PhotoManager.getPhoto(EMPTY_PATH);
        assertThat(photoUriEmptyPath, is(uri));

        Uri photoUriInvalidPath = PhotoManager.getPhoto(INVALID_PATH);
        assertThat(photoUriInvalidPath, is(uri));
    }

}