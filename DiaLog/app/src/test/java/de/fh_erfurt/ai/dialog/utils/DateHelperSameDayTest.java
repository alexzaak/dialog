package de.fh_erfurt.ai.dialog.utils;


import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author Alexander Zaak
 * @version 1.0
 * @since 20.07.17
 */
public class DateHelperSameDayTest {

    private static final Date TODAY_NOON;
    private static final Date TOMORROW_NOON;
    private static final Date YESTERDAY_NOON;

    private static final Date INVALID_DATE_NULL = null;
    private static final Date INVALID_DATE_HOURS_25;
    private static final Date INVALID_DATE_NEGATIVE_HOURS;

    static {
        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.HOUR_OF_DAY, 12);
        TODAY_NOON = calendar.getTime();

        calendar.add(Calendar.DAY_OF_MONTH, +1);
        TOMORROW_NOON = calendar.getTime();

        calendar.setTime(TODAY_NOON);
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        YESTERDAY_NOON = calendar.getTime();

        calendar.setTime(TODAY_NOON);
        calendar.set(Calendar.HOUR_OF_DAY, 25);
        INVALID_DATE_HOURS_25 = calendar.getTime();

        calendar.setTime(TODAY_NOON);
        calendar.set(Calendar.HOUR_OF_DAY, -1);
        INVALID_DATE_NEGATIVE_HOURS = calendar.getTime();
    }

    @Test
    public void sameDay() throws Exception {
        assertThat(DateHelper.sameDay(TODAY_NOON, TODAY_NOON), is(true));
    }

    @Test
    public void sameDayNight() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(TODAY_NOON);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        Date todayNight = calendar.getTime();

        assertThat(DateHelper.sameDay(TODAY_NOON, todayNight), is(true));
        assertThat(DateHelper.sameDay(todayNight, TODAY_NOON), is(true));
    }

    @Test
    public void sameDayNightOneMilli() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(TODAY_NOON);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 1);

        Date todayNight = calendar.getTime();

        assertThat(DateHelper.sameDay(TODAY_NOON, todayNight), is(true));
        assertThat(DateHelper.sameDay(todayNight, TODAY_NOON), is(true));
    }

    @Test
    public void sameDayMidnight() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(TODAY_NOON);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 59);

        Date todayMidnight = calendar.getTime();

        assertThat(DateHelper.sameDay(TODAY_NOON, todayMidnight), is(true));
        assertThat(DateHelper.sameDay(todayMidnight, TODAY_NOON), is(true));
    }

    @Test
    public void sameDayInvalidDateNull() throws Exception {
        assertThat(DateHelper.sameDay(TODAY_NOON, INVALID_DATE_NULL), is(false));
        assertThat(DateHelper.sameDay(INVALID_DATE_NULL, TODAY_NOON), is(false));
    }

    @Test
    public void sameDayInvalidDateHours25() throws Exception {
        assertThat(DateHelper.sameDay(TODAY_NOON, INVALID_DATE_HOURS_25), is(false));
        assertThat(DateHelper.sameDay(INVALID_DATE_HOURS_25, TODAY_NOON), is(false));
    }

    @Test
    public void notSameDayTomorrow() throws Exception {
        assertThat(DateHelper.sameDay(TODAY_NOON, TOMORROW_NOON), is(false));
        assertThat(DateHelper.sameDay(TOMORROW_NOON, TODAY_NOON), is(false));
    }

    @Test
    public void notSameDayYesterday() throws Exception {
        assertThat(DateHelper.sameDay(TODAY_NOON, YESTERDAY_NOON), is(false));
        assertThat(DateHelper.sameDay(YESTERDAY_NOON, TODAY_NOON), is(false));
    }

    @Test
    public void notSameDayNegativeHours() throws Exception {
        assertThat(DateHelper.sameDay(TODAY_NOON, INVALID_DATE_NEGATIVE_HOURS), is(false));
        assertThat(DateHelper.sameDay(INVALID_DATE_NEGATIVE_HOURS, TODAY_NOON), is(false));
    }
}