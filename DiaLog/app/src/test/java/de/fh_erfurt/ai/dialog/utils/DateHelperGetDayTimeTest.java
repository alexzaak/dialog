package de.fh_erfurt.ai.dialog.utils;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Calendar;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

/**
 * Created by alex on 22.07.17.
 */

@RunWith(PowerMockRunner.class)
@PrepareForTest({Calendar.class, DateHelper.class})
public class DateHelperGetDayTimeTest {

    private static final int YEAR = 2017;
    private static final int MONTH = Calendar.JULY;
    private static final int DAY = 22;

    private static final int MORNING = -1;
    private static final int NOON = 0;
    private static final int EVENING = 1;

    private static final Calendar CALENDAR = Calendar.getInstance();

    @Before
    public void steUp() {
        mockStatic(Calendar.class);
        when(Calendar.getInstance()).thenReturn(CALENDAR);
    }

    @Test
    public void testGetDayTimeMorning() throws Exception {
        CALENDAR.set(YEAR, MONTH, DAY, 0, 0);
        assertThat(DateHelper.getDayTime(), is(MORNING));

        CALENDAR.set(YEAR, MONTH, DAY, 10, 59, 59);
        assertThat(DateHelper.getDayTime(), is(MORNING));

        CALENDAR.set(YEAR, MONTH, DAY, 5, 31);
        assertThat(DateHelper.getDayTime(), is(MORNING));
    }

    @Test
    public void testGetDayTimeNoon() throws Exception {
        CALENDAR.set(YEAR, MONTH, DAY, 11, 0);
        assertThat(DateHelper.getDayTime(), is(NOON));

        CALENDAR.set(YEAR, MONTH, DAY, 13, 59, 59);
        assertThat(DateHelper.getDayTime(), is(NOON));

        CALENDAR.set(YEAR, MONTH, DAY, 11, 1, 1);
        assertThat(DateHelper.getDayTime(), is(NOON));
    }

    @Test
    public void testGetDayTimeEvening() throws Exception {
        CALENDAR.set(YEAR, MONTH, DAY, 14, 0, 0);
        assertThat(DateHelper.getDayTime(), is(EVENING));

        CALENDAR.set(YEAR, MONTH, DAY, 23, 59, 59);
        assertThat(DateHelper.getDayTime(), is(EVENING));

        CALENDAR.set(YEAR, MONTH, DAY, 20, 15, 59);
        assertThat(DateHelper.getDayTime(), is(EVENING));
    }
}
