package de.fh_erfurt.ai.dialog.utils;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.test.mock.MockContext;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import de.fh_erfurt.ai.dialog.BuildConfig;
import de.fh_erfurt.ai.dialog.TestApplication;
import de.fh_erfurt.ai.dialog.controller.App;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

/**
 * Created by alex on 23.07.17.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = Build.VERSION_CODES.LOLLIPOP, application = TestApplication.class)
@PrepareForTest({Activity.class, ContextCompat.class})
public class PermissionsCheckTest {

    private static final String[] PERMISSION_TAKE_PHOTO = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA};

    private static final String[] PERMISSION_TAKE_PHOTO_2 = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA};

    private static final String[] NO_PERMISSIONS_REQUIRED = {};

    private static final int PERMISSIONS_REQUEST_CODE = 3101;

    private Activity mockedActivity;

    @Mock
    private App app;

    @Mock
    private MockContext mockedContext = new MockContext();

    @Before
    public void setUp() {
        mockedActivity = mock(Activity.class);
    }

    @Config(constants = BuildConfig.class, sdk = Build.VERSION_CODES.M)
    @Test
    public void testCheckRequiredPermissionsAllGranted() throws Exception {
        when(mockedActivity.checkPermission(anyString(), anyInt(), anyInt())).thenReturn(PackageManager.PERMISSION_GRANTED);

        String[] permissions = PermissionsCheck.checkRequiredPermissions(mockedActivity, PERMISSION_TAKE_PHOTO);

        assertThat(permissions, is(equalTo(NO_PERMISSIONS_REQUIRED)));
    }

    @Config(constants = BuildConfig.class, sdk = Build.VERSION_CODES.M)
    @Test
    public void testCheckRequiredPermissionsAllDenied() throws Exception {
        when(mockedActivity.checkPermission(anyString(), anyInt(), anyInt())).thenReturn(PackageManager.PERMISSION_DENIED);

        String[] permissions = PermissionsCheck.checkRequiredPermissions(mockedActivity, PERMISSION_TAKE_PHOTO);

        assertThat(permissions, is(equalTo(PERMISSION_TAKE_PHOTO)));
    }

    @Config(constants = BuildConfig.class, sdk = Build.VERSION_CODES.M)
    @Test
    public void testCheckRequiredPermissionsGrantedDenied() throws Exception {
        when(mockedActivity.checkPermission(eq(Manifest.permission.WRITE_EXTERNAL_STORAGE), anyInt(), anyInt())).thenReturn(PackageManager.PERMISSION_DENIED);
        when(mockedActivity.checkPermission(eq(Manifest.permission.READ_EXTERNAL_STORAGE), anyInt(), anyInt())).thenReturn(PackageManager.PERMISSION_GRANTED);
        when(mockedActivity.checkPermission(eq(Manifest.permission.CAMERA), anyInt(), anyInt())).thenReturn(PackageManager.PERMISSION_DENIED);

        String[] permissions = PermissionsCheck.checkRequiredPermissions(mockedActivity, PERMISSION_TAKE_PHOTO);

        assertThat(permissions, is(equalTo(PERMISSION_TAKE_PHOTO_2)));
    }


    @Config(constants = BuildConfig.class, sdk = Build.VERSION_CODES.LOLLIPOP)
    @Test
    public void testRequestPermissionsLessAndroidM() throws Exception {
        PermissionsCheck.requestPermissions(mockedActivity, PERMISSION_TAKE_PHOTO);
        verifyNoMoreInteractions(mockedActivity);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Config(constants = BuildConfig.class, sdk = Build.VERSION_CODES.M)
    @Test
    public void testRequestPermissionAndroidM() throws Exception {
        PermissionsCheck.requestPermissions(mockedActivity, PERMISSION_TAKE_PHOTO);

        verify(mockedActivity, times(1)).requestPermissions(PERMISSION_TAKE_PHOTO, PERMISSIONS_REQUEST_CODE);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Config(constants = BuildConfig.class, sdk = Build.VERSION_CODES.M)
    @Test
    public void testRequestPermissionAndroidMAllGranted() throws Exception {
        PermissionsCheck.requestPermissions(mockedActivity, NO_PERMISSIONS_REQUIRED);

        verify(mockedActivity, never()).requestPermissions(NO_PERMISSIONS_REQUIRED, PERMISSIONS_REQUEST_CODE);
    }
}