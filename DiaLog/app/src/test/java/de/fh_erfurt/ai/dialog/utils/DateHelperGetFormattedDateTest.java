package de.fh_erfurt.ai.dialog.utils;

import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

/**
 * Created by alex on 21.07.17.
 */

public class DateHelperGetFormattedDateTest {

    private static final int YEAR = 2017;
    private static final int DAY = 21;
    private static final int HOUR = 12;
    private static final int MINUTE = 0;

    private static final Locale LOCALE_GERMAN;
    private static final Locale LOCALE_ENGLISH;

    private static final String EXPECTED_DATE_FORMAT = "21.07.2017";
    private static final String EXPECTED_TIME_FORMAT = "12:00";
    private static final String EXPECTED_DATE_DAY_FORMAT_GERMAN = "Freitag, 21.07.2017";

    private static final String EXPECTED_DATE_FORMAT_ENGLISH = "Jul 21, 2017";
    private static final String EXPECTED_DATE_DAY_FORMAT_ENGLISH = "Friday, 21.07.2017";

    private static final Date INVALID_DATE_NULL = null;

    private static final String INVALID_FORMAT_NULL = null;
    private static final String INVALID_FORMAT_EMPTY = "";
    private static final String INVALID_FORMAT = "FH:GB";

    private static final Date TODAY_NOON;

    static {
        LOCALE_GERMAN = new Locale("de", "DE");
        LOCALE_ENGLISH = new Locale("en", "US");

        Calendar calendar = Calendar.getInstance();

        calendar.set(YEAR, Calendar.JULY, DAY, HOUR, MINUTE);
        TODAY_NOON = calendar.getTime();
    }

    @Before
    public void setUp() {
        Locale.setDefault(LOCALE_GERMAN);
    }

    @Test
    public void getFormattedDateLocaleGerman() throws Exception {
        String formattedDate = DateHelper.getFormattedDate(TODAY_NOON);
        assertThat(formattedDate, is(equalTo(EXPECTED_DATE_FORMAT)));
    }

    @Test
    public void getFormattedDateLocaleEnglish() throws Exception {
        Locale.setDefault(LOCALE_ENGLISH);
        String formattedDate = DateHelper.getFormattedDate(TODAY_NOON);
        assertThat(formattedDate, is(equalTo(EXPECTED_DATE_FORMAT_ENGLISH)));
    }

    @Test
    public void getFormattedDateInvalidDate() throws Exception {
        String formattedDate = DateHelper.getFormattedDate(INVALID_DATE_NULL);
        assertThat(formattedDate, is(equalTo(null)));
    }

    @Test
    public void getFormattedDateFormatGerman() throws Exception {
        String formattedTime = DateHelper.getFormattedDate(TODAY_NOON, "HH:mm");
        assertThat(formattedTime, is(equalTo(EXPECTED_TIME_FORMAT)));

        String formattedDate = DateHelper.getFormattedDate(TODAY_NOON, "dd.MM.yyyy");
        assertThat(formattedDate, is(equalTo(EXPECTED_DATE_FORMAT)));


        String formattedDateDay = DateHelper.getFormattedDate(TODAY_NOON, "EEEE, dd.MM.yyyy");
        assertThat(formattedDateDay, is(equalTo(EXPECTED_DATE_DAY_FORMAT_GERMAN)));
    }

    @Test
    public void getFormattedDateFormatEnglish() throws Exception {
        Locale.setDefault(LOCALE_ENGLISH);
        String formattedTime = DateHelper.getFormattedDate(TODAY_NOON, "HH:mm");
        assertThat(formattedTime, is(equalTo(EXPECTED_TIME_FORMAT)));

        String formattedDate = DateHelper.getFormattedDate(TODAY_NOON, "dd.MM.yyyy");
        assertThat(formattedDate, is(equalTo(EXPECTED_DATE_FORMAT)));


        String formattedDateDay = DateHelper.getFormattedDate(TODAY_NOON, "EEEE, dd.MM.yyyy");
        assertThat(formattedDateDay, is(equalTo(EXPECTED_DATE_DAY_FORMAT_ENGLISH)));
    }

    @Test
    public void getFormattedDateFormatDateInvalid() throws Exception {
        String formattedTime = DateHelper.getFormattedDate(INVALID_DATE_NULL, "HH:mm");
        assertThat(formattedTime, is(not(equalTo(EXPECTED_TIME_FORMAT))));
    }

    @Test
    public void getFormattedDateFormatNull() throws Exception {
        String formattedTime = DateHelper.getFormattedDate(TODAY_NOON, INVALID_FORMAT_NULL);
        assertThat(formattedTime, is(equalTo(null)));
    }

    @Test
    public void getFormattedDateFormatEmpty() throws Exception {
        String formattedTime = DateHelper.getFormattedDate(TODAY_NOON, INVALID_FORMAT_EMPTY);
        assertThat(formattedTime, is(equalTo(null)));
    }

    @Test
    public void getFormattedDateFormatInvalid() throws Exception {
        String formattedTime = DateHelper.getFormattedDate(TODAY_NOON, INVALID_FORMAT);
        assertThat(formattedTime, is(equalTo(null)));
    }
}
