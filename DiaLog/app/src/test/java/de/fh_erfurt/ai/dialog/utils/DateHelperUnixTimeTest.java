package de.fh_erfurt.ai.dialog.utils;

import org.exparity.hamcrest.date.DateMatchers;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author Alexander Zaak
 * @version 1.0
 * @since 12.02.18
 */
public class DateHelperUnixTimeTest {

    private static final int YEAR = 2017;
    private static final int DAY = 13;
    private static final int HOUR = 12;
    private static final int MINUTE = 45;

    private static final TimeZone TIME_ZONE_UTC = TimeZone.getTimeZone("UTC");

    private static final long TEST_UNIX_TIME_STAMP = 1499949900L;

    private static final Date EXPECTED_DATE;
    private static final long EXPECTED_UNIX_TIME_STAMP = 1499949900000L;

    private static final long INVALID_UNIX_TIME_STAMP_SEC_LATER = 1499949901L;
    private static final long INVALID_UNIX_TIME_STAMP_SEX_EARLIER = 1499949899L;
    private static final long INVALID_UNIX_TIME_STAMP_ZERO = 0L;
    private static final long INVALID_UNIX_TIME_STAMP_NEGATIVE = -1L;

    static {
        Calendar calendar = Calendar.getInstance();

        calendar.set(YEAR, Calendar.JULY, DAY, HOUR, MINUTE, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.setTimeZone(TIME_ZONE_UTC);
        EXPECTED_DATE = calendar.getTime();
    }

    @Test
    public void fromUnixTime() throws Exception {
        Date actualDate = DateHelper.fromUnixTime(TEST_UNIX_TIME_STAMP);
        assertThat(actualDate, DateMatchers.sameInstant(EXPECTED_DATE));
    }

    @Test
    public void fromUnixTimeInvalidZero() throws Exception {
        Date actualDate = DateHelper.fromUnixTime(INVALID_UNIX_TIME_STAMP_ZERO);

        Calendar calendar = Calendar.getInstance();

        calendar.set(1970, Calendar.JANUARY, 1, 1, 0, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        assertThat(actualDate, DateMatchers.sameInstant(calendar.getTime()));
    }

    @Test
    public void fromUnixTimeInvalidNegativeValue() throws Exception {
        Date actualDate = DateHelper.fromUnixTime(INVALID_UNIX_TIME_STAMP_NEGATIVE);

        Calendar calendar = Calendar.getInstance();

        calendar.set(1970, Calendar.JANUARY, 1, 0, 59, 59);
        calendar.set(Calendar.MILLISECOND, 0);

        assertThat(actualDate, DateMatchers.sameInstant(calendar.getTime()));
    }

    @Test
    public void fromUnixTimeInvalidMax() throws Exception {
        Date actualDate = DateHelper.fromUnixTime(Long.MAX_VALUE);

        Calendar calendar = Calendar.getInstance();

        calendar.set(1970, Calendar.JANUARY, 1, 0, 59, 59);
        calendar.set(Calendar.MILLISECOND, 0);

        assertThat(actualDate, DateMatchers.sameInstant(calendar.getTime()));
    }

    @Test
    public void fromUnixTimeInvalidMin() throws Exception {
        Date actualDate = DateHelper.fromUnixTime(Long.MIN_VALUE);

        Calendar calendar = Calendar.getInstance();

        calendar.set(1970, Calendar.JANUARY, 1, 1, 0, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        assertThat(actualDate, DateMatchers.sameInstant(calendar.getTime()));
    }

    @Test
    public void toUnixTime() throws Exception {
        long actualUnixTimeStamp = DateHelper.toUnixTime(EXPECTED_DATE);
        assertThat(actualUnixTimeStamp, is(EXPECTED_UNIX_TIME_STAMP));
    }

    @Test
    public void toUnixTimeInvalidDateNull() throws Exception {
        long actualUnixTimeStamp = DateHelper.toUnixTime(null);
        assertThat(actualUnixTimeStamp, is(0L));
    }
}

