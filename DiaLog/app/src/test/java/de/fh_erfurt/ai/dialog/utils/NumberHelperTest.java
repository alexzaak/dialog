package de.fh_erfurt.ai.dialog.utils;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author Alexander Zaak
 * @version 1.0
 * @since 13.02.18
 */
public class NumberHelperTest {

    private static final String EXPECTED_DOUBLE_MAX_VALUE = "179769313486231570000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";


    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void formatNumber() throws Exception {
        NumberHelper numberHelper = new NumberHelper();

        String formattedValue1 = numberHelper.format(9.003);
        assertThat(formattedValue1, is(equalTo("9")));

        String formattedValue2 = numberHelper.format(9.130);
        assertThat(formattedValue2, is(equalTo("9.1")));

        String formattedValue3 = numberHelper.format(0.030);
        assertThat(formattedValue3, is(equalTo("0")));

        String formattedValue4 = numberHelper.format(-1.030);
        assertThat(formattedValue4, is(equalTo("-1")));

        String formattedValue5 = numberHelper.format(Double.MAX_VALUE);
        assertThat(formattedValue5, is(equalTo(EXPECTED_DOUBLE_MAX_VALUE)));

        String formattedValue6 = numberHelper.format(Double.MIN_VALUE);
        assertThat(formattedValue6, is(equalTo("0")));
    }


    @Test
    public void doubleToString() throws Exception {
       String actualValue = NumberHelper.toString(9.00);
        assertThat(actualValue, is(equalTo("9.0")));

        String actualValue1 = NumberHelper.toString(-9.00);
        assertThat(actualValue1, is(equalTo("-9.0")));

        String actualValue3 = NumberHelper.toString(9.000001);
        assertThat(actualValue3, is(equalTo("9.000001")));

        String actualValue4 = NumberHelper.toString(0.000000);
        assertThat(actualValue4, is(equalTo("0.0")));
    }
}