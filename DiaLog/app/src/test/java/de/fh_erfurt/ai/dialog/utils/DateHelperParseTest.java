package de.fh_erfurt.ai.dialog.utils;

import org.exparity.hamcrest.date.DateMatchers;
import org.joda.time.DateTimeZone;
import org.joda.time.tz.UTCProvider;
import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author Alexander Zaak
 * @version 1.0
 * @since 13.02.18
 */
public class DateHelperParseTest {

    private static final String VALID_DATE_ISO = "2017-07-13T12:45:00Z";
    private static final String VALID_DATE_UTC = "2017-07-13T12:45:00+02:00";

    private static final String INVALID_DATE_NULL = null;
    private static final String INVALID_MALFORMED_DATE = "2017-d7-13T12:45:00+02:00";

    private static final int YEAR = 2017;
    private static final int DAY = 13;
    private static final int HOUR = 12;
    private static final int MINUTE = 45;

    private static final TimeZone TIME_ZONE_UTC = TimeZone.getTimeZone("UTC");
    private static final TimeZone DEFAULT_TIME_ZONE = TimeZone.getDefault();

    private static final Calendar TEST_CALENDAR = Calendar.getInstance();

    @Before
    public void setUp() {
        TEST_CALENDAR.set(YEAR, Calendar.JULY, DAY, HOUR, MINUTE, 0);
        TEST_CALENDAR.set(Calendar.MILLISECOND, 0);
        TEST_CALENDAR.setTimeZone(DEFAULT_TIME_ZONE);
        DateTimeZone.setProvider(new UTCProvider());
    }

    @Test
    public void parseISODate() throws Exception {
        TEST_CALENDAR.setTimeZone(TIME_ZONE_UTC);
        Date expectedDate = TEST_CALENDAR.getTime();
        Date actualDate = DateHelper.parse(VALID_DATE_ISO);
        assertThat(actualDate, DateMatchers.sameInstant(expectedDate));
    }

    @Test
    public void parseUTCDate() throws Exception {
        Date expectedDate = TEST_CALENDAR.getTime();

        Date actualDate = DateHelper.parse(VALID_DATE_UTC);
        assertThat(actualDate, DateMatchers.sameInstant(expectedDate));
    }

    @Test
    public void parseInvalidNullValue() throws Exception {
        Date actualDate = DateHelper.parse(INVALID_DATE_NULL);
        assertThat(actualDate, is(equalTo(null)));
    }

    @Test
    public void parseMalformedDate() throws Exception {
        Date actualDate = DateHelper.parse(INVALID_MALFORMED_DATE);
        assertThat(actualDate, is(equalTo(null)));
    }
}
