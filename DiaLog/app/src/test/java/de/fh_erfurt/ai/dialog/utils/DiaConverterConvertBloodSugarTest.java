package de.fh_erfurt.ai.dialog.utils;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.UnknownFormatConversionException;

import de.fh_erfurt.ai.dialog.enums.BloodsugarUnit;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.number.IsCloseTo.closeTo;
import static org.junit.Assert.assertThat;

/**
 * Created by max on 23.07.17.
 */

public class DiaConverterConvertBloodSugarTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    public static final double TEST_MMOL_L_VALUE = 5d;
    public static final double TEST_MG_DL_VALUE = 90.091d;

    @Test
    public void testConvertBloodSugarFromToEquals() {
        double convertedValue = DiaConverter.convertBloodSugar(BloodsugarUnit.MMOL_L, BloodsugarUnit.MMOL_L, TEST_MMOL_L_VALUE);
        assertThat(convertedValue, is(TEST_MMOL_L_VALUE));
    }

    @Test
    public void testConvertBloodSugarMMOLL2MGDL() {
        double convertedValue = DiaConverter.convertBloodSugar(BloodsugarUnit.MMOL_L, BloodsugarUnit.MG_DL, TEST_MMOL_L_VALUE);
        assertThat(convertedValue, is(closeTo(TEST_MG_DL_VALUE, 0.001d)));
    }

    @Test
    public void testConvertBloodSugarMGDL2MMOLL() {
        double convertedValue = DiaConverter.convertBloodSugar(BloodsugarUnit.MG_DL, BloodsugarUnit.MMOL_L, TEST_MG_DL_VALUE);
        assertThat(convertedValue, is(closeTo(TEST_MMOL_L_VALUE, 0.001d)));
    }

    @Test
    public void testConvertBloodSugarUnknownConversionMGDLNull() {
        thrown.expect(UnknownFormatConversionException.class);
        thrown.expectMessage("Can not convert MG_DL to null");
        DiaConverter.convertBloodSugar(BloodsugarUnit.MG_DL, null, TEST_MG_DL_VALUE);
    }

    @Test
    public void testConvertBloodSugarUnknownConversionNullMMOLL() {
        thrown.expect(UnknownFormatConversionException.class);
        thrown.expectMessage("Can not convert null to MMOL_L");
        DiaConverter.convertBloodSugar(null, BloodsugarUnit.MMOL_L, TEST_MG_DL_VALUE);
    }
}
