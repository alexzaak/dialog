package de.fh_erfurt.ai.dialog.adapter;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.powermock.modules.junit4.rule.PowerMockRule;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import de.fh_erfurt.ai.dialog.BuildConfig;
import de.fh_erfurt.ai.dialog.TestApplication;
import de.fh_erfurt.ai.dialog.controller.UserSettingsManager;
import de.fh_erfurt.ai.dialog.enums.BloodsugarUnit;
import de.fh_erfurt.ai.dialog.enums.CarbsUnit;
import de.fh_erfurt.ai.dialog.food.model.FoodPageableResponse;
import de.fh_erfurt.ai.dialog.food.model.FoodResponse;
import de.fh_erfurt.ai.dialog.model.User;

import static org.junit.Assert.assertEquals;

/**
 * @author Alexander Zaak
 * @version 1.0
 * @since 18.02.18
 */
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = Build.VERSION_CODES.LOLLIPOP, application = TestApplication.class)
@PowerMockIgnore({"org.mockito.*", "org.robolectric.*", "android.*", "com.sun.org.apache.xerces.*"})
@SuppressStaticInitializationFor("io.realm.internal.Util")
@PrepareForTest(UserSettingsManager.class)
public class FoodAdapterTest {

    private static final FoodResponse TEST_FOOD_1 = new FoodResponse();
    private static final FoodResponse TEST_FOOD_2 = new FoodResponse();
    private static final FoodPageableResponse TEST_PAGEABLE_1 = new FoodPageableResponse();
    private static final FoodPageableResponse TEST_PAGEABLE_2 = new FoodPageableResponse();

    private static final User TEST_USER = new User();


    private Context mContext;

    @Rule
    public PowerMockRule rule = new PowerMockRule();

    @Before
    public void setup() {

        PowerMockito.mockStatic(UserSettingsManager.class);
        Mockito.when(UserSettingsManager.getUser()).thenReturn(TEST_USER);

        mContext = RuntimeEnvironment.application;

        TEST_FOOD_1.setCarbs(10);
        TEST_FOOD_1.setCreatedAt(new Date());
        TEST_FOOD_1.setId(UUID.randomUUID().toString());
        TEST_FOOD_1.setName("Banana");

        TEST_FOOD_2.setCarbs(5);
        TEST_FOOD_2.setCreatedAt(new Date());
        TEST_FOOD_2.setId(UUID.randomUUID().toString());
        TEST_FOOD_2.setName("Noodle");

        TEST_PAGEABLE_1.setElements(Collections.singletonList(TEST_FOOD_1));
        TEST_PAGEABLE_1.setPage(1);
        TEST_PAGEABLE_1.setSize(1);
        TEST_PAGEABLE_1.setTotalElements(1);
        TEST_PAGEABLE_1.setTotalPages(1);

        TEST_PAGEABLE_2.setElements(Arrays.asList(TEST_FOOD_1, TEST_FOOD_2));
        TEST_PAGEABLE_2.setPage(1);
        TEST_PAGEABLE_2.setSize(2);
        TEST_PAGEABLE_2.setTotalElements(2);
        TEST_PAGEABLE_2.setTotalPages(2);


        // Set up a test User
        TEST_USER.setName("Max");

        TEST_USER.setThresholdHyper(10f);
        TEST_USER.setThresholdHypo(5f);
        TEST_USER.setBolusFactorCorrection(2.5f);

        TEST_USER.setBolusFactorMorning(2f);
        TEST_USER.setBolusFactorNoon(2.2f);
        TEST_USER.setBolusFactorEvening(1.5f);

        TEST_USER.setTargetAreaFrom(4.5f);
        TEST_USER.setTargetAreaTo(8.5f);
        TEST_USER.setBloodSugarUnit(BloodsugarUnit.MMOL_L.getId());
        TEST_USER.setCarbsUnit(CarbsUnit.CU.getId());
    }


    @Test
    public void foodAdapterItemViewHolder() {
        // Set up input
        List<FoodResponse> posts = Arrays.asList(
                TEST_FOOD_1,
                TEST_FOOD_2);

        FoodAdapter adapter = new FoodAdapter(mContext, posts, null);

        RecyclerView rvParent = new RecyclerView(mContext);
        rvParent.setLayoutManager(new LinearLayoutManager(mContext));

        // Run test
        FoodAdapter.ItemViewHolder viewHolder = (FoodAdapter.ItemViewHolder) adapter.onCreateViewHolder(rvParent, 0);

        adapter.onBindViewHolder(viewHolder, 0);

        // JUnit Assertion
        assertEquals(View.VISIBLE, viewHolder.carbsUnit.getVisibility());
        assertEquals(View.VISIBLE, viewHolder.foodCarbs.getVisibility());
        assertEquals(View.VISIBLE, viewHolder.foodName.getVisibility());
        assertEquals(View.VISIBLE, viewHolder.photo.getVisibility());
    }

    @Test
    public void foodAdapterEmptyViewHolder() {

        // Set up input
        List<FoodResponse> posts = new ArrayList<>();

        FoodAdapter adapter = new FoodAdapter(mContext, posts, null);

        RecyclerView rvParent = new RecyclerView(mContext);
        rvParent.setLayoutManager(new LinearLayoutManager(mContext));

        // Run test
        FoodAdapter.EmptyViewHolder viewHolder = (FoodAdapter.EmptyViewHolder) adapter.onCreateViewHolder(rvParent, 2);

        adapter.onBindViewHolder(viewHolder, 0);

        // JUnit Assertion
        assertEquals(View.VISIBLE, viewHolder.itemView.getVisibility());
    }

    @Test
    public void foodAdapterFooterViewHolder() {

        // Set up input
        List<FoodResponse> posts = new ArrayList<>();

        FoodAdapter adapter = new FoodAdapter(mContext, posts, null);

        RecyclerView rvParent = new RecyclerView(mContext);
        rvParent.setLayoutManager(new LinearLayoutManager(mContext));

        // Run test
        FoodAdapter.FooterViewHolder viewHolder = (FoodAdapter.FooterViewHolder) adapter.onCreateViewHolder(rvParent, 1);

        adapter.onBindViewHolder(viewHolder, 0);

        // JUnit Assertion
        assertEquals(View.VISIBLE, viewHolder.loadMoreBtn.getVisibility());
    }

    @Test
    public void foodAdapterItemCountEmptyState() {

        // Set up input
        List<FoodResponse> posts = new ArrayList<>();

        FoodAdapter adapter = new FoodAdapter(mContext, posts, null);

        // JUnit Assertion
        assertEquals(1, adapter.getItemCount());
    }

    @Test
    public void foodAdapterItemCountItems() {

        // Set up input
        List<FoodResponse> posts = Arrays.asList(TEST_FOOD_1, TEST_FOOD_2);

        FoodAdapter adapter = new FoodAdapter(mContext, posts, null);

        // JUnit Assertion
        assertEquals(3, adapter.getItemCount());
    }

    @Test
    public void foodAdapterSmartUpdate() {

        // Set up input
        List<FoodResponse> posts = new ArrayList<>();

        FoodAdapter adapter = new FoodAdapter(mContext, posts, null);

        adapter.add(TEST_PAGEABLE_1);

        assertEquals(2, adapter.getItemCount());

        adapter.add(TEST_PAGEABLE_2);

        assertEquals(4, adapter.getItemCount());
    }

    @Test
    public void foodAdapterViewType() {

        // Set up input
        List<FoodResponse> posts = new ArrayList<>();

        FoodAdapter adapter = new FoodAdapter(mContext, posts, null);

        assertEquals(2, adapter.getItemViewType(0));

        adapter.add(TEST_PAGEABLE_2);

        assertEquals(0, adapter.getItemViewType(0));
        assertEquals(0, adapter.getItemViewType(1));
        assertEquals(1, adapter.getItemViewType(2));
    }
}