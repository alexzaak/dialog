package de.fh_erfurt.ai.dialog.utils;

import org.junit.Before;
import org.junit.Test;

import de.fh_erfurt.ai.dialog.enums.BloodsugarUnit;
import de.fh_erfurt.ai.dialog.enums.CarbsUnit;
import de.fh_erfurt.ai.dialog.model.User;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.number.IsCloseTo.closeTo;
import static org.junit.Assert.assertThat;

/**
 * Created by max on 23.07.17.
 */

public class BolusCalcCorrectionTest {

    private BolusCalc bolusCalc;

    private static final User TEST_USER = new User();

    private static final double TEST_CORRECTION_FACTOR = 2d;

    private static final double TEST_TARGET_AREA_FROM = 4.5d;
    private static final double TEST_TARGET_AREA_TO = 8.5d;

    private static final double EXPECTED_CORRECTION_IN_TARGET_AREA = 0d;
    private static final double EXPECTED_CORRECTION_LOW = 0d;
    private static final double EXPECTED_CORRECTION_HIGH = 1.05d;
    private static final double EXPECTED_CORRECTION_VERY_HIGH = 1.75d;

    @Before
    public void setUp() {
        TEST_USER.setBolusFactorCorrection(TEST_CORRECTION_FACTOR);

        TEST_USER.setTargetAreaFrom(TEST_TARGET_AREA_FROM);
        TEST_USER.setTargetAreaTo(TEST_TARGET_AREA_TO);
        TEST_USER.setBloodSugarUnit(BloodsugarUnit.MMOL_L.getId());
        TEST_USER.setCarbsUnit(CarbsUnit.CU.getId());

        this.bolusCalc = new BolusCalc(TEST_USER);
    }

    @Test
    public void testCalcCorrectionInTargetArea() {
        double correction = this.bolusCalc.calcCorrection(4.5d);
        assertThat(correction, is(equalTo(EXPECTED_CORRECTION_IN_TARGET_AREA)));

        correction = this.bolusCalc.calcCorrection(8.5d);
        assertThat(correction, is(equalTo(EXPECTED_CORRECTION_IN_TARGET_AREA)));

        correction = this.bolusCalc.calcCorrection(6d);
        assertThat(correction, is(equalTo(EXPECTED_CORRECTION_IN_TARGET_AREA)));
    }

    @Test
    public void testCalcCorrectionLow() {
        double correction = this.bolusCalc.calcCorrection(4.49d);
        assertThat(correction, is(equalTo(EXPECTED_CORRECTION_LOW)));

        correction = this.bolusCalc.calcCorrection(2.0d);
        assertThat(correction, is(equalTo(EXPECTED_CORRECTION_LOW)));
    }

    @Test
    public void testCalcCorrectionHigh() {
        double correction = this.bolusCalc.calcCorrection(8.6d);
        //correction = round(correction * 100) / 100;
        assertThat(correction, is(closeTo(EXPECTED_CORRECTION_HIGH, 0.01d)));

        correction = this.bolusCalc.calcCorrection(10d);
        assertThat(correction, is(closeTo(EXPECTED_CORRECTION_VERY_HIGH, 0.01d)));
    }

    @Test
    public void testCalcCorrectionInvalidFactor() {
        TEST_USER.setBolusFactorCorrection(0);
        double correction = this.bolusCalc.calcCorrection(6d);

        assertThat(correction, is(equalTo(0d)));
    }
}
