package de.fh_erfurt.ai.dialog.controller;

import android.content.Context;
import android.os.Build;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.rule.PowerMockRule;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.UUID;

import de.fh_erfurt.ai.dialog.BuildConfig;
import de.fh_erfurt.ai.dialog.TestApplication;
import de.fh_erfurt.ai.dialog.enums.BloodsugarUnit;
import de.fh_erfurt.ai.dialog.enums.CarbsUnit;
import de.fh_erfurt.ai.dialog.model.User;
import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.log.RealmLog;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * Created by alex on 22.07.17.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = Build.VERSION_CODES.LOLLIPOP, application = TestApplication.class)
@PowerMockIgnore({"org.mockito.*", "org.robolectric.*", "android.*"})
@SuppressStaticInitializationFor("io.realm.internal.Util")
@PrepareForTest({UUID.class, Realm.class, RealmLog.class, UserSettingsManager.class, RealmObject.class})
public class UserSettingsManagerTest {

    private static final User TEST_USER = new User();
    private static final String IMG_FILE_NAME = "img_221190.jpg";
    private static final String TEST_USER_ID = "fe7f0ab5-65eb-48d7-9b2e-c3d2ef71fe3b";

    @Rule
    public PowerMockRule rule = new PowerMockRule();

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    private Realm mockRealm;

    private RealmQuery<User> userQuery;

    @Before
    public void setUp() throws Exception {
        Context ctx = RuntimeEnvironment.application;

        mockStatic(RealmLog.class);
        mockStatic(Realm.class);
        mockStatic(UUID.class);
        mockStatic(RealmObject.class);

        Realm mockRealm = PowerMockito.mock(Realm.class);
        when(Realm.getDefaultInstance()).thenReturn(mockRealm);
        when(UUID.randomUUID().toString()).thenReturn(TEST_USER_ID);

        this.mockRealm = mockRealm;
        this.userQuery = mockRealmQuery();
        when(mockRealm.where(User.class)).thenReturn(this.userQuery);

        byte[] imgBytes = new byte[512];

        // Set up a test User
        TEST_USER.setName("Max");
        TEST_USER.setProfileImage(imgBytes);
        TEST_USER.setPhotoName(IMG_FILE_NAME);

        TEST_USER.setThresholdHyper(10f);
        TEST_USER.setThresholdHypo(5f);
        TEST_USER.setBolusFactorCorrection(2.5f);

        TEST_USER.setBolusFactorMorning(2f);
        TEST_USER.setBolusFactorNoon(2.2f);
        TEST_USER.setBolusFactorEvening(1.5f);

        TEST_USER.setTargetAreaFrom(4.5f);
        TEST_USER.setTargetAreaTo(8.5f);
        TEST_USER.setBloodSugarUnit(BloodsugarUnit.MMOL_L.getId());
        TEST_USER.setCarbsUnit(CarbsUnit.CU.getId());
    }

    @Test
    public void testRealmInstance() throws Exception {
        assertThat(Realm.getDefaultInstance(), is(this.mockRealm));
    }

    @Test
    public void testCreateUser() throws Exception {
        User user = UserSettingsManager.createUser();
        User mockedUser = this.mockRealm.createObject(User.class);

        assertThat(mockedUser, is(user));
    }

    @Test
    public void testVerifyUserCreated() throws Exception {
        User mockedUser = mock(User.class);
        when(this.mockRealm.createObject(User.class, TEST_USER_ID)).thenReturn(mockedUser);

        UserSettingsManager.createUser();

        verify(this.mockRealm, times(1)).createObject(User.class, TEST_USER_ID);
        verify(this.mockRealm, times(1)).close();
    }

    @Test
    public void testGetUser() throws Exception {
        User mockedUser = mock(User.class);
        when(this.userQuery.findFirst()).thenReturn(mockedUser);
        when(mockedUser.isValid()).thenReturn(true);

        UserSettingsManager.getUser();

        verify(this.mockRealm, times(1)).copyFromRealm(mockedUser);
        verify(this.mockRealm, times(1)).close();
    }

    @Test
    public void testGetManagedUserExists() throws Exception {
        when(this.userQuery.findFirst()).thenReturn(TEST_USER);
        when(TEST_USER.isValid()).thenReturn(true);

        UserSettingsManager.getManagedUser();

        verify(this.mockRealm, times(1)).where(User.class);
        verify(this.userQuery, times(1)).findFirst();
        verify(this.mockRealm, never()).close();
    }

    @Test
    public void testGetManagedUserNotExists() throws Exception {
        when(this.userQuery.findFirst()).thenReturn(null);

        UserSettingsManager.getManagedUser();

        verify(this.mockRealm, times(1)).where(User.class);
        verify(this.userQuery, times(1)).findFirst();
        verify(this.mockRealm, times(1)).createObject(User.class, TEST_USER_ID);
        verify(this.mockRealm, times(1)).close();
    }

    @Test
    public void testGetManagedUserNotValid() throws Exception {
        User mockedUser = mock(User.class);
        when(this.userQuery.findFirst()).thenReturn(mockedUser);
        when(mockedUser.isValid()).thenReturn(false);

        UserSettingsManager.getManagedUser();

        verify(this.mockRealm, times(1)).where(User.class);
        verify(this.userQuery, times(1)).findFirst();
        verify(this.mockRealm, times(1)).createObject(User.class, TEST_USER_ID);
        verify(this.mockRealm, times(1)).close();
    }

    @Test
    public void testSaveUserSettings() throws Exception {
        UserSettingsManager.saveUserSettings(TEST_USER);

        verify(this.mockRealm, times(1)).beginTransaction();
        verify(this.mockRealm, times(1)).copyToRealmOrUpdate(TEST_USER);
        verify(this.mockRealm, times(1)).commitTransaction();
        verify(this.mockRealm, times(1)).close();
    }

    @SuppressWarnings("unchecked")
    private <T extends RealmObject> RealmQuery<T> mockRealmQuery() {
        return mock(RealmQuery.class);
    }
}