package de.fh_erfurt.ai.dialog.utils;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.UnknownFormatConversionException;

import de.fh_erfurt.ai.dialog.enums.CarbsUnit;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.number.IsCloseTo.closeTo;
import static org.junit.Assert.assertThat;

/**
 * Created by max on 23.07.17.
 */

public class DiaConverterConvertCarbsTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    public static final double TEST_CARBS_CU_VALUE = 5d;
    public static final double TEST_CARBS_BU_VALUE = 4.17d;

    @Test
    public void testConvertCarbsFromToEquals() {
        double convertedValue = DiaConverter.convertCarbs(CarbsUnit.CU, CarbsUnit.CU, TEST_CARBS_CU_VALUE);
        assertThat(convertedValue, is(TEST_CARBS_CU_VALUE));
    }

    @Test
    public void testConvertCarbsCUBU() {
        double convertedValue = DiaConverter.convertCarbs(CarbsUnit.CU, CarbsUnit.BU, TEST_CARBS_CU_VALUE);
        assertThat(convertedValue, is(closeTo(TEST_CARBS_BU_VALUE, 0.01d)));
    }

    @Test
    public void testConvertCarbsBUCU() {
        double convertedValue = DiaConverter.convertCarbs(CarbsUnit.BU, CarbsUnit.CU, TEST_CARBS_BU_VALUE);
        assertThat(convertedValue, is(closeTo(TEST_CARBS_CU_VALUE, 0.01d)));
    }

    @Test
    public void testConvertBloodSugarUnknownConversionMGDLNull() {
        thrown.expect(UnknownFormatConversionException.class);
        thrown.expectMessage("Can not convert BU to null");
        DiaConverter.convertCarbs(CarbsUnit.BU, null, TEST_CARBS_BU_VALUE);
    }

    @Test
    public void testConvertBloodSugarUnknownConversionNullMMOLL() {
        thrown.expect(UnknownFormatConversionException.class);
        thrown.expectMessage("Can not convert null to CU");
        DiaConverter.convertCarbs(null, CarbsUnit.CU, TEST_CARBS_BU_VALUE);
    }
}
