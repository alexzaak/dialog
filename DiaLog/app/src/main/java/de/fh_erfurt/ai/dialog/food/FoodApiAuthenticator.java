package de.fh_erfurt.ai.dialog.food;

import android.content.Context;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.Tasks;

import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import de.fh_erfurt.ai.dialog.food.model.AuthRequest;
import de.fh_erfurt.ai.dialog.food.model.AuthResponse;
import de.fh_erfurt.ai.dialog.utils.PrefUtils;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Class to authenticate the client before every request
 *
 * @author Alexander Zaak
 * @version 1.0
 * @since 07.01.18
 */
public class FoodApiAuthenticator {

    private static final String X_REALM_ACCESS_TOKEN = "X-REALM-ACCESS-TOKEN";

    private static final Executor SINGLE = Executors.newSingleThreadExecutor();

    private final PrefUtils prefUtils;

    private final Context ctx;

    public FoodApiAuthenticator(final Context ctx) {
        this.ctx = ctx;
        this.prefUtils = new PrefUtils(ctx);

    }

    /**
     * Method to check if client is authorized, else refresh the token
     *
     * @return Task
     */
    public Task<Void> check() {
        return Tasks.call(SINGLE, new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                final TaskCompletionSource<Void> source = new TaskCompletionSource<>();

                final String token = prefUtils.getGoogleToken();
                final long expireAt = prefUtils.getExpireAt();
                final String provider = prefUtils.getProvider();

                if (token == null) {
                    source.setException(new Exception("provider is null"));
                } else if (System.currentTimeMillis() > expireAt) {
                    FoodApi.getService(ctx).auth(getRequestBody(token, provider)).enqueue(new Callback<AuthResponse>() {
                        @Override
                        public void onResponse(Call<AuthResponse> call, Response<AuthResponse> response) {
                            Timber.d("Auth error");
                            if (!response.isSuccessful()) {
                                source.setException(new Exception("unable to authenticate"));
                                return;
                            }

                            final AuthResponse body = response.body();
                            if (body == null || body.refreshToken.token == null) {
                                source.setException(new Exception("token is null"));
                                return;
                            }

                            init(body.refreshToken.token, body.refreshToken.tokenData.expires);
                            source.setResult(null);
                        }

                        @Override
                        public void onFailure(Call<AuthResponse> call, Throwable t) {
                            source.setException(new Exception(t));
                        }
                    });
                } else {
                    source.setResult(null);
                }

                return Tasks.await(source.getTask());
            }
        });
    }

    /**
     * Method to set authorization header
     *
     * @param builder builder
     */
    public void authenticate(final Request.Builder builder) {
        final String token = this.prefUtils.getAccessToken();
        if (token != null) {
            builder.addHeader(X_REALM_ACCESS_TOKEN, token);
        }
    }

    private void init(final String token, final long expires) {
        this.prefUtils.setAccessToken(token);
        this.prefUtils.setExpireAt(System.currentTimeMillis() + expires * 1000);
    }

    private AuthRequest getRequestBody(final String token, final String provider) {
        final AuthRequest request = new AuthRequest();
        request.setData(token);
        request.setProvider(provider);
        return request;
    }
}
