package de.fh_erfurt.ai.dialog.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.fh_erfurt.ai.dialog.FoodActivity;
import de.fh_erfurt.ai.dialog.R;
import de.fh_erfurt.ai.dialog.adapter.FoodAdapter;
import de.fh_erfurt.ai.dialog.food.FoodApi;
import de.fh_erfurt.ai.dialog.food.model.FoodPageableResponse;
import de.fh_erfurt.ai.dialog.food.model.FoodResponse;
import de.fh_erfurt.ai.dialog.utils.ViewUtils;
import timber.log.Timber;

/**
 * Created by alexander on 10.01.18.
 */

public class SearchFoodFragment extends Fragment implements FoodAdapter.OnInteractionListener {

    private static final int TYPE_FETCH_FOOD_DATA = 0;
    private static final int TYPE_SHOW_FOOD_DATA = 1;

    private static final int FIRST_PAGE = 0;
    private static final int ITEM_LIMIT = 5;
    private static final String SAVE_STATE_SEARCH_KEY = "save_state_search_key";

    public interface OnInteractionListener {
        void onAddBtnPressed(final String searchKey);

        void onFoodSelected(final double carbs);
    }

    private final Handler handler = new MyHandler(this);

    private Unbinder unbinder;
    private Validator validator;
    private FoodAdapter adapter;

    private OnInteractionListener listener;
    private String searchKey;

    @NotEmpty(messageResId = R.string.validation_msg_required_field)
    @BindView(R.id.input_search_key)
    TextInputEditText searchInput;

    @BindView(R.id.btn_search_food)
    ImageButton searchBtn;

    @BindView(R.id.btn_add_food)
    ImageButton addBtn;

    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.food_list)
    RecyclerView foodListView;

    public SearchFoodFragment() {
        //
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_food, container, false);
        this.unbinder = ButterKnife.bind(this, view);

        this.adapter = new FoodAdapter(getActivity(), new ArrayList<FoodResponse>(), this);
        this.foodListView.setAdapter(adapter);
        this.foodListView.setLayoutManager(new LinearLayoutManager(getActivity()));
        this.foodListView.setNestedScrollingEnabled(false);

        this.searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search(v);
            }
        });

        this.searchInput.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() != KeyEvent.ACTION_DOWN)
                    return false;
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    search(v);
                    return true;
                }
                return false;
            }
        });

        this.addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String searchKey = searchInput.getText().toString();
                if (listener != null) {
                    listener.onAddBtnPressed(searchKey);
                }
            }
        });

        this.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                Message.obtain(handler, TYPE_FETCH_FOOD_DATA, 0, ITEM_LIMIT, searchKey).sendToTarget();
            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState != null) {
            this.searchInput.setText(savedInstanceState.getString(SAVE_STATE_SEARCH_KEY));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(SAVE_STATE_SEARCH_KEY, this.searchInput.getText().toString());
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                break;
        }
        return true;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.listener = (FoodActivity) context;

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();

        if (actionBar != null) {
            actionBar.setTitle(R.string.title_food_search);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.listener = null;
        this.handler.removeCallbacksAndMessages(null);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        this.unbinder.unbind();
    }

    @Override
    public void onMoreBtnClick(final int nextPage) {
        swipeRefreshLayout.setRefreshing(true);
        Message.obtain(handler, TYPE_FETCH_FOOD_DATA, nextPage, ITEM_LIMIT, searchKey).sendToTarget();
    }

    @Override
    public void onItemClick(FoodResponse food) {
        this.listener.onFoodSelected(food.getCarbs());
    }

    private void search(View v) {
        if (searchInput != null && searchInput.getText() != null) {
            swipeRefreshLayout.setRefreshing(true);
            searchKey = searchInput.getText().toString();
            Message.obtain(handler, TYPE_FETCH_FOOD_DATA, FIRST_PAGE, ITEM_LIMIT, searchKey).sendToTarget();
            ViewUtils.hideKeyboard(getActivity(), v);
        }
    }

    private static class MyHandler extends Handler {

        private final WeakReference<SearchFoodFragment> ref;

        private MyHandler(final SearchFoodFragment fragment) {
            super(Looper.getMainLooper());
            ref = new WeakReference<>(fragment);
        }

        @Override
        public void dispatchMessage(final Message msg) {
            super.dispatchMessage(msg);

            final SearchFoodFragment fragment = ref.get();
            if (fragment == null || fragment.getActivity().isFinishing()) {
                return;
            }

            if (msg.obj == null) {
                fragment.swipeRefreshLayout.setRefreshing(false);
                return;
            }

            switch (msg.what) {
                case TYPE_FETCH_FOOD_DATA: {

                    final String searchKey = msg.obj.toString();
                    final int page = msg.arg1;
                    final int limit = msg.arg2;

                    FoodApi.search(fragment.getActivity(), searchKey, page, limit).addOnCompleteListener(new OnCompleteListener<FoodPageableResponse>() {
                        @Override
                        public void onComplete(@NonNull Task<FoodPageableResponse> task) {
                            if (task.isSuccessful()) {
                                obtainMessage(TYPE_SHOW_FOOD_DATA, task.getResult()).sendToTarget();
                            } else {
                                Timber.e("get food list failed");
                            }
                            fragment.swipeRefreshLayout.setRefreshing(false);
                        }
                    });
                    break;
                }
                case TYPE_SHOW_FOOD_DATA: {
                    final FoodPageableResponse result = (FoodPageableResponse) msg.obj;
                    fragment.adapter.add(result);
                    break;
                }
            }
        }
    }
}
