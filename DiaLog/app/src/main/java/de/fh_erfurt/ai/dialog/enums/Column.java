package de.fh_erfurt.ai.dialog.enums;

/**
 * Enumeration for column names in database
 *
 * @author Alexander Zaak
 * @version 1.0
 * @since 31.05.17
 */
public enum Column {
    CREATED_AT("createdAt"), BLOODSUGAR("bloodSugar"), CARBS("carbs"), BOLUS("bolus"), THRESHOLD_HYPER("thresholdHyper");

    private String tableName;

    Column(String tableName) {
        this.tableName = tableName;
    }

    public String getName() {
        return this.tableName;
    }
}
