package de.fh_erfurt.ai.dialog.fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.DecimalMax;
import com.mobsandgeeks.saripaar.annotation.DecimalMin;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.fh_erfurt.ai.dialog.NfcConfigActivity;
import de.fh_erfurt.ai.dialog.R;
import de.fh_erfurt.ai.dialog.controller.LogManager;
import de.fh_erfurt.ai.dialog.controller.UserSettingsManager;
import de.fh_erfurt.ai.dialog.enums.BloodsugarUnit;
import de.fh_erfurt.ai.dialog.enums.CarbsUnit;
import de.fh_erfurt.ai.dialog.model.User;
import de.fh_erfurt.ai.dialog.utils.NumberHelper;
import de.fh_erfurt.ai.dialog.utils.PhotoManager;
import de.fh_erfurt.ai.dialog.utils.ViewUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment implements Validator.ValidationListener, OnRangeSeekbarChangeListener {

    private User user;

    @NotEmpty(messageResId = R.string.validation_msg_required_field)
    @BindView(R.id.input_username)
    TextInputEditText usernameInput;

    @NotEmpty(messageResId = R.string.validation_msg_required_field)
    @DecimalMin(value = 0.01f, messageResId = R.string.validation_msg_greater_zero)
    @DecimalMax(value = 5.0, messageResId = R.string.validation_msg_less_five)
    @BindView(R.id.input_morning)
    TextInputEditText bolusFactorMorningInput;

    @NotEmpty(messageResId = R.string.validation_msg_required_field)
    @DecimalMin(value = 0.01f, messageResId = R.string.validation_msg_greater_zero)
    @DecimalMax(value = 5.0, messageResId = R.string.validation_msg_less_five)
    @BindView(R.id.input_noon)
    TextInputEditText bolusFactorNoonInput;

    @NotEmpty(messageResId = R.string.validation_msg_required_field)
    @DecimalMin(value = 0.01f, messageResId = R.string.validation_msg_greater_zero)
    @DecimalMax(value = 5.0, messageResId = R.string.validation_msg_less_five)
    @BindView(R.id.input_evening)
    TextInputEditText bolusFactorEveningInput;

    @NotEmpty(messageResId = R.string.validation_msg_required_field)
    @DecimalMin(value = 0.01f, messageResId = R.string.validation_msg_greater_zero)
    @DecimalMax(value = 5.0, messageResId = R.string.validation_msg_less_five)
    @BindView(R.id.input_correction)
    TextInputEditText bolusFactorCorrectionInput;

    @NotEmpty(messageResId = R.string.validation_msg_required_field)
    @DecimalMin(value = 8f, messageResId = R.string.validation_msg_greater_eight)
    @DecimalMax(value = 15f, messageResId = R.string.validation_msg_less_fifteen)
    @BindView(R.id.input_threshold_hyper)
    TextInputEditText thresholdHyperInput;

    @NotEmpty(messageResId = R.string.validation_msg_required_field)
    @DecimalMin(value = 0.01f, messageResId = R.string.validation_msg_greater_zero)
    @DecimalMax(value = 4f, messageResId = R.string.validation_msg_less_four)
    @BindView(R.id.input_threshold_hypo)
    TextInputEditText thresholdHypoInput;


    @BindView(R.id.imageview_profile_picture)
    ImageView profilePictureImageView;

    @BindView(R.id.radio_group_bloodsugar_unit)
    RadioGroup bloodSugarUnitRadioGroup;

    @BindView(R.id.radio_group_carbs_unit)
    RadioGroup carbsUnitRadioGroup;

    @BindView(R.id.seek_bar_target_area)
    CrystalRangeSeekbar targetAreaSeekBar;

    @BindView(R.id.seek_bar_text_left)
    TextView seekBarTextLeft;

    @BindView(R.id.seek_bar_text_right)
    TextView seekBarTextRight;

    @BindView(R.id.btn_start_app)
    Button autoStartButton;

    private Unbinder unbinder;
    private Validator validator;

    private BloodsugarUnit currentBloodSugarUnit;
    private CarbsUnit currentCarbsUnit;

    private NumberHelper numberHelper;


    public SettingsFragment() {
        setHasOptionsMenu(true);
        this.user = UserSettingsManager.getUser();
        this.currentBloodSugarUnit = BloodsugarUnit.getUnit(this.user.getBloodSugarUnit());
        this.currentCarbsUnit = CarbsUnit.getUnit(this.user.getCarbsUnit());

        this.numberHelper = new NumberHelper();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        this.unbinder = ButterKnife.bind(this, view);

        setUserSettings();

        this.validator = new Validator(this);
        this.validator.setValidationListener(this);

        this.targetAreaSeekBar.setOnRangeSeekbarChangeListener(this);

        this.autoStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), NfcConfigActivity.class));
            }
        });

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (this.unbinder != null) {
            this.unbinder.unbind();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_create_log, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_save) {
            this.validator.validate();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri photo = PhotoManager.getPhoto(requestCode, resultCode);
        if (photo != null) {
            final File imageFile = PhotoManager.getFile(photo);
            final byte[] image = PhotoManager.getBytes(getActivity(), photo);

            this.user.setProfileImage(image);
            this.user.setPhotoName(imageFile.getName());
            UserSettingsManager.saveUserSettings(user);

            Picasso.with(getContext()).load(imageFile).into(this.profilePictureImageView);
        }
    }

    @OnClick(R.id.imageview_profile_picture)
    void onCameraStart() {
        try {
            PhotoManager.startCamera(getActivity(), SettingsFragment.this);
        } catch (IOException e) {
            Toast.makeText(this.getContext(), getString(R.string.error_dir_creation), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onValidationSucceeded() {
        UserSettingsManager.saveUserSettings(getUserSettings());

        BloodsugarUnit newBloodSugarUnit = BloodsugarUnit.getUnit(this.user.getBloodSugarUnit());
        CarbsUnit newCarbsUnit = CarbsUnit.getUnit(this.user.getCarbsUnit());
        LogManager.convertLog(this.currentBloodSugarUnit, newBloodSugarUnit, this.currentCarbsUnit, newCarbsUnit);
        this.currentBloodSugarUnit = newBloodSugarUnit;
        this.currentCarbsUnit = newCarbsUnit;

        this.showSnackbar(getString(R.string.message_user_profile_saved));

        ViewUtils.hideKeyboard(getActivity(), this.getView());
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        this.showSnackbar(getString(R.string.validation_msg_error));
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this.getActivity());

            // Display error messages ;)
            if (view instanceof TextInputEditText) {
                ((TextInputEditText) view).setError(message);
            } else {
                Toast.makeText(this.getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void valueChanged(Number minValue, Number maxValue) {
        this.seekBarTextLeft.setText(this.numberHelper.format(minValue.floatValue()));
        this.seekBarTextRight.setText(this.numberHelper.format(maxValue.floatValue()));
    }

    private void showSnackbar(String message) {
        if (getView() != null) {
            Snackbar.make(getView(), message, Snackbar.LENGTH_LONG).show();
        }
    }

    private void setUserSettings() {
        this.usernameInput.setText(this.user.getName());

        this.bolusFactorMorningInput.setText(NumberHelper.toString(this.user.getBolusFactorMorning()));
        this.bolusFactorNoonInput.setText(NumberHelper.toString(this.user.getBolusFactorNoon()));
        this.bolusFactorEveningInput.setText(NumberHelper.toString(this.user.getBolusFactorEvening()));
        this.bolusFactorCorrectionInput.setText(NumberHelper.toString(this.user.getBolusFactorCorrection()));

        this.thresholdHyperInput.setText(NumberHelper.toString(this.user.getThresholdHyper()));
        this.thresholdHypoInput.setText(NumberHelper.toString(this.user.getThresholdHypo()));

        int bloodSugarUnitRadioId = BloodsugarUnit.getUnit(this.user.getBloodSugarUnit()).getRadioRes();
        int carbsUnitRadioId = CarbsUnit.getUnit(this.user.getCarbsUnit()).getRadioRes();
        this.bloodSugarUnitRadioGroup.check(bloodSugarUnitRadioId);
        this.carbsUnitRadioGroup.check(carbsUnitRadioId);

        this.targetAreaSeekBar.setMinStartValue((float) this.user.getTargetAreaFrom());
        this.targetAreaSeekBar.setMaxStartValue((float) this.user.getTargetAreaTo());
        this.targetAreaSeekBar.setSteps(0.2f).setGap(0.5f).apply();

        this.seekBarTextLeft.setText(this.numberHelper.format(this.user.getTargetAreaFrom()));
        this.seekBarTextRight.setText(this.numberHelper.format(this.user.getTargetAreaTo()));

        PhotoManager.setPhoto(getActivity(), this.profilePictureImageView, this.user.getProfileImage(), this.user.getPhotoName());
    }

    private User getUserSettings() {
        this.user.setName(this.usernameInput.getText().toString());
        this.user.setBolusFactorMorning(NumberHelper.getDoubleValue(bolusFactorMorningInput));
        this.user.setBolusFactorNoon(NumberHelper.getDoubleValue(bolusFactorNoonInput));
        this.user.setBolusFactorEvening(NumberHelper.getDoubleValue(bolusFactorEveningInput));
        this.user.setBolusFactorCorrection(NumberHelper.getDoubleValue(bolusFactorCorrectionInput));
        this.user.setBloodSugarUnit(BloodsugarUnit.getUnitId(bloodSugarUnitRadioGroup.getCheckedRadioButtonId()));
        this.user.setCarbsUnit(CarbsUnit.getUnitId(carbsUnitRadioGroup.getCheckedRadioButtonId()));

        this.user.setTargetAreaFrom(this.targetAreaSeekBar.getSelectedMinValue().floatValue());
        this.user.setTargetAreaTo(this.targetAreaSeekBar.getSelectedMaxValue().floatValue());

        this.user.setThresholdHyper(NumberHelper.getDoubleValue(thresholdHyperInput));
        this.user.setThresholdHypo(NumberHelper.getDoubleValue(thresholdHypoInput));

        return this.user;
    }
}
