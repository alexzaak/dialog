package de.fh_erfurt.ai.dialog.controller;

import java.util.Date;

import de.fh_erfurt.ai.dialog.enums.BloodsugarUnit;
import de.fh_erfurt.ai.dialog.enums.CarbsUnit;
import de.fh_erfurt.ai.dialog.enums.Column;
import de.fh_erfurt.ai.dialog.model.LogEntry;
import de.fh_erfurt.ai.dialog.utils.DateHelper;
import de.fh_erfurt.ai.dialog.utils.DiaConverter;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * LogManager
 * Database manager for log entries.
 *
 * @author Max Edenharter
 */
public class LogManager {

    /**
     * Returns a single log entry by its ID.
     *
     * @param id The ID of the log entry.
     * @return The log entry.
     */
    public static LogEntry getLogEntry(String id) {
        Realm realm = Realm.getDefaultInstance();
        LogEntry entry = realm.copyFromRealm(realm.where(LogEntry.class).equalTo("id", id).findFirst());
        realm.close();
        return entry;
    }

    /**
     * Returns all log entries.
     *
     * @return All log entries.
     */
    public static RealmResults<LogEntry> getAllLogEntries() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<LogEntry> results = realm.where(LogEntry.class).findAll();
        realm.close();
        return results;
    }

    /**
     * Persists a log entry.
     *
     * @param logEntry The log entry to persist.
     */
    public static void saveLogEntry(LogEntry logEntry) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(logEntry);
        realm.commitTransaction();
        realm.close();
    }

    /**
     * Removes a log entry from database.
     *
     * @param logEntry The log entry to remove.
     */
    public static void deleteLogEntry(LogEntry logEntry) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.where(LogEntry.class).equalTo("id", logEntry.getId()).findFirst().deleteFromRealm();
        realm.commitTransaction();
        realm.close();
    }

    /**
     * Returns the averaged value of the specified column/property of log entries in the given time period.
     *
     * @param column The column/property
     * @param from   Start date.
     * @param to     End date.
     * @return Averaged value.
     */
    public static double getAveragedValue(Column column, Date from, Date to) {
        Realm realm = Realm.getDefaultInstance();
        double value;
        realm.beginTransaction();
        value = realm.where(LogEntry.class).between(Column.CREATED_AT.getName(), from, to).average(column.getName());
        realm.commitTransaction();
        realm.close();
        return value;
    }

    /**
     * Returns the summed value of the specified column/property of log entries in the given time period.
     *
     * @param column The column/property.
     * @param from   Start date
     * @param to     End date
     * @return Summed value.
     */
    public static double getSummedValue(Column column, Date from, Date to) {
        Realm realm = Realm.getDefaultInstance();
        double value;
        realm.beginTransaction();
        value = realm.where(LogEntry.class).between(Column.CREATED_AT.getName(), from, to).sum(column.getName()).doubleValue();
        realm.commitTransaction();
        realm.close();
        return value;
    }

    /**
     * Returns the amount of hypers for the current day.
     *
     * @return Count of hypers.
     */
    public static long getHypersCount() {
        Realm realm = Realm.getDefaultInstance();
        double thresholdHyper = UserSettingsManager.getThresholdHyper();
        long count;
        realm.beginTransaction();
        count = realm.where(LogEntry.class).between(Column.CREATED_AT.getName(), DateHelper.dayStarted(new Date()), DateHelper.dayEnded(new Date())).greaterThan(Column.BLOODSUGAR.getName(), thresholdHyper).count();
        realm.commitTransaction();
        realm.close();
        return count;
    }

    /**
     * Returns the amount of hypos for the current day.
     *
     * @return Count of hypos.
     */
    public static long getHyposCount() {
        Realm realm = Realm.getDefaultInstance();
        double thresholdHypo = UserSettingsManager.getThresholdHypo();
        long count;
        realm.beginTransaction();
        count = realm.where(LogEntry.class).between(Column.CREATED_AT.getName(), DateHelper.dayStarted(new Date()), DateHelper.dayEnded(new Date())).lessThan(Column.BLOODSUGAR.getName(), thresholdHypo).count();
        realm.commitTransaction();
        realm.close();
        return count;
    }

    /**
     * Converts the entire log bloodsugar values and carbs values from one unit to another.
     *
     * @param fromBU The source bloodsugar unit.
     * @param toBU   The target bloodsugar unit.
     * @param fromCU The source carbs unit.
     * @param toCU   The target carbs unit.
     */
    public static void convertLog(BloodsugarUnit fromBU, BloodsugarUnit toBU, CarbsUnit fromCU, CarbsUnit toCU) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        RealmResults<LogEntry> logEntries = realm.where(LogEntry.class).findAll();
        for (LogEntry logEntry : logEntries) {
            logEntry.setBloodSugar(DiaConverter.convertBloodSugar(fromBU, toBU, logEntry.getBloodSugar()));
            logEntry.setCarbs(DiaConverter.convertCarbs(fromCU, toCU, logEntry.getCarbs()));
            realm.copyToRealmOrUpdate(logEntry);
        }
        realm.commitTransaction();
        realm.close();
    }
}
