package de.fh_erfurt.ai.dialog.fragment;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.mobsandgeeks.saripaar.QuickRule;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.DecimalMin;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Pattern;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.fh_erfurt.ai.dialog.FoodActivity;
import de.fh_erfurt.ai.dialog.R;
import de.fh_erfurt.ai.dialog.controller.UserSettingsManager;
import de.fh_erfurt.ai.dialog.enums.CarbsUnit;
import de.fh_erfurt.ai.dialog.food.FoodApi;
import de.fh_erfurt.ai.dialog.food.model.FoodRequest;
import de.fh_erfurt.ai.dialog.food.model.FoodResponse;
import de.fh_erfurt.ai.dialog.model.User;
import de.fh_erfurt.ai.dialog.service.ImageUploadService;
import de.fh_erfurt.ai.dialog.utils.DiaConverter;
import de.fh_erfurt.ai.dialog.utils.NumberHelper;
import de.fh_erfurt.ai.dialog.utils.PhotoManager;

/**
 * Fragment to add a new food entry
 *
 * @author Alexander Zaak
 * @version 1.0
 * @since 10.01.18
 */
public class AddFoodFragment extends Fragment implements Validator.ValidationListener {

    private static final String EXTRA_FOOD_NAME = "extra-food-name";
    private static final String SAVE_STATE_FOOD_NAME = "save_state_food_name";
    private static final String SAVE_STATE_IMAGE = "save_state_image";
    private static final String SAVE_STATE_CARBS = "save_state_carbs";

    public interface OnInteractionListener {
        void onFoodAdded(final double carbs);

        void onCancel();
    }

    private User userSettings;
    private String foodName;
    private Unbinder unbinder;
    private Validator validator;
    private OnInteractionListener mListener;

    private Uri imageFile;

    @NotEmpty(messageResId = R.string.validation_msg_required_field)
    @Length(min = 4, max = 100, messageResId = R.string.validation_msg_string_length)
    @Pattern(regex = "[A-z0-9ÄäÖöÜüß\\s]+", messageResId = R.string.validation_msg_alphanumeric)
    @BindView(R.id.input_name)
    TextInputEditText nameInput;

    @NotEmpty(messageResId = R.string.validation_msg_required_field)
    @DecimalMin(value = 0.f, messageResId = R.string.validation_msg_greater_zero)
    @BindView(R.id.input_carbs)
    TextInputEditText carbsInput;

    @BindView(R.id.imageview_food_photo)
    ImageView foodImageView;

    @BindView(R.id.textview_carbs_unit)
    TextView carbsUnit;

    public AddFoodFragment() {
        this.userSettings = UserSettingsManager.getUser();
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            foodName = bundle.getString(EXTRA_FOOD_NAME, "");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_food, container, false);
        this.unbinder = ButterKnife.bind(this, view);

        this.validator = new Validator(this);
        this.validator.setValidationListener(this);


        if (foodName != null) {
            this.nameInput.setText(foodName);
        }
        this.foodImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startCamera();
            }
        });

        CarbsUnit currentUnit = CarbsUnit.getUnit(userSettings.getCarbsUnit());
        this.carbsUnit.setText(getString(currentUnit.getNameRes()));

        this.validator.put(carbsInput, new DecimalMaxRule(currentUnit));

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState != null) {
            this.nameInput.setText(savedInstanceState.getString(SAVE_STATE_FOOD_NAME));
            this.carbsInput.setText(String.valueOf(savedInstanceState.getDouble(SAVE_STATE_CARBS)));

            final String uriString = savedInstanceState.getString(SAVE_STATE_IMAGE);
            if (uriString != null) {
                this.imageFile = Uri.parse(uriString);
                Picasso.with(this.getContext()).load(this.imageFile).into(this.foodImageView);
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(SAVE_STATE_FOOD_NAME, this.nameInput.getText().toString());
        outState.putDouble(SAVE_STATE_CARBS, NumberHelper.getDoubleValue(this.carbsInput));

        if (this.imageFile != null) {
            outState.putString(SAVE_STATE_IMAGE, this.imageFile.toString());
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (this.unbinder != null) {
            this.unbinder.unbind();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_create_log, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (this.mListener != null) {
                    this.mListener.onCancel();
                }
                break;

            case R.id.action_save:
                this.validator.validate();
                break;
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri photo = PhotoManager.getPhoto(requestCode, resultCode);
        if (photo != null) {
            this.imageFile = photo;
            Picasso.with(this.getContext()).load(photo).into(this.foodImageView);
        }
    }

    @OnClick(R.id.btn_take_photo)
    void onCameraStart() {
        this.startCamera();
    }

    @Override
    public void onValidationSucceeded() {
        final String name = this.nameInput.getText().toString();
        final double carbs = NumberHelper.getDoubleValue(this.carbsInput);

        final CarbsUnit currentCarbsUnit = CarbsUnit.getUnit(this.userSettings.getCarbsUnit());
        final double convertedResult = DiaConverter.convertCarbs(currentCarbsUnit, CarbsUnit.CU, carbs);

        FoodRequest food = new FoodRequest(name, convertedResult);
        FoodApi.addFood(getActivity(), food).addOnCompleteListener(new OnCompleteListener<FoodResponse>() {
            @Override
            public void onComplete(@NonNull Task<FoodResponse> task) {
                if (task.isSuccessful()) {
                    if (mListener != null) {

                        String id = task.getResult().getId();
                        final double carbs = task.getResult().getCarbs();
                        mListener.onFoodAdded(carbs);

                        ImageUploadService.startService(id, imageFile);
                    }
                    AddFoodFragment.this.showSnackbar(getString(R.string.message_food_added));
                } else {
                    AddFoodFragment.this.showSnackbar(getString(R.string.message_api_error));
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                AddFoodFragment.this.showSnackbar(getString(R.string.message_api_error));
            }
        });
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        this.showSnackbar(getString(R.string.validation_msg_error));
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this.getActivity());

            // Display error messages ;)
            if (view instanceof TextInputEditText) {
                ((TextInputEditText) view).setError(message);
            } else {
                Toast.makeText(this.getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mListener = (FoodActivity) context;

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();

        if (actionBar != null) {
            actionBar.setTitle(R.string.title_add_food);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.mListener = null;
    }

    private void showSnackbar(String message) {
        if (getView() != null) {
            Snackbar.make(getView(), message, Snackbar.LENGTH_LONG).show();
        }
    }

    private void startCamera() {
        try {
            PhotoManager.startCamera(getActivity(), AddFoodFragment.this);
        } catch (IOException e) {
            Toast.makeText(this.getContext(), getString(R.string.error_dir_creation), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Method to create new instance with arguments
     *
     * @param foodName food name
     * @return new instance
     */
    public static AddFoodFragment create(final String foodName) {
        AddFoodFragment fragment = new AddFoodFragment();
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_FOOD_NAME, foodName);
        fragment.setArguments(bundle);
        return fragment;
    }

    /**
     * Custom validation rule to validate the current value for the carbs by the usersettings
     */
    private class DecimalMaxRule extends QuickRule<EditText> {
        private static final double MAX_CU = 10;
        private static final double MAX_BU = 8.3333;

        private final CarbsUnit currentUnit;

        public DecimalMaxRule(final CarbsUnit currentUnit) {
            this.currentUnit = currentUnit;
        }

        @Override
        public boolean isValid(EditText editText) {
            final double value = NumberHelper.getDoubleValue(editText);

            switch (this.currentUnit) {
                case BU:
                    if (value <= MAX_BU) {
                        return true;
                    }
                    break;
                case CU:
                    if (value <= MAX_CU) {
                        return true;
                    }
                    break;
            }

            return false;
        }

        @Override
        public String getMessage(Context context) {
            switch (this.currentUnit) {
                case BU:
                    return context.getString(R.string.validation_msg_less_eight);
                case CU:
                    return context.getString(R.string.validation_msg_less_ten);
                default:
                    return context.getString(R.string.validation_msg_generic);
            }
        }
    }
}
