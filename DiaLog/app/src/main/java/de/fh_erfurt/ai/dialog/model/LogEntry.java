package de.fh_erfurt.ai.dialog.model;

import java.util.Date;
import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Model class for log entries.
 *
 * @author Max Edenharter
 */
public class LogEntry extends RealmObject {
    @PrimaryKey
    private String id = UUID.randomUUID().toString();

    private Date createdAt = new Date();
    private double bloodSugar;
    private double carbs;
    private double bolus;
    private double correction;
    private long activity;
    private String notes;

    public String getId() {
        return id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public double getBloodSugar() {
        return bloodSugar;
    }

    public void setBloodSugar(double bloodSugar) {
        this.bloodSugar = bloodSugar;
    }

    public double getCarbs() {
        return carbs;
    }

    public void setCarbs(double carbs) {
        this.carbs = carbs;
    }

    public double getBolus() {
        return bolus;
    }

    public void setBolus(double bolus) {
        this.bolus = bolus;
    }

    public double getCorrection() {
        return correction;
    }

    public void setCorrection(double correction) {
        this.correction = correction;
    }

    public long getActivity() {
        return this.activity;
    }

    public void setActivity(long activity) {
        this.activity = activity;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Override
    public String toString() {
        return "LogEntry{" +
                "id='" + id + '\'' +
                ", createdAt=" + createdAt +
                ", bloodSugar=" + bloodSugar +
                ", carbs=" + carbs +
                ", bolus=" + bolus +
                ", correction=" + correction +
                ", activity=" + activity +
                '}';
    }
}
