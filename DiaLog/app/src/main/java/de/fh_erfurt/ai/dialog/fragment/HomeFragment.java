package de.fh_erfurt.ai.dialog.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.fh_erfurt.ai.dialog.R;
import de.fh_erfurt.ai.dialog.controller.LogManager;
import de.fh_erfurt.ai.dialog.controller.UserSettingsManager;
import de.fh_erfurt.ai.dialog.enums.BloodsugarUnit;
import de.fh_erfurt.ai.dialog.enums.CarbsUnit;
import de.fh_erfurt.ai.dialog.enums.Column;
import de.fh_erfurt.ai.dialog.model.User;
import de.fh_erfurt.ai.dialog.utils.DateHelper;
import de.fh_erfurt.ai.dialog.utils.PhotoManager;
import io.realm.RealmChangeListener;

/**
 * A placeholder fragment containing a simple view.
 */
public class HomeFragment extends Fragment {

    @BindView(R.id.textview_date)
    TextView dateText;

    @BindView(R.id.textview_bloodsugar_value)
    TextView bloodsugarValueText;

    @BindView(R.id.textview_bloodsugar_unit)
    TextView bloodsugarUnitText;

    @BindView(R.id.textview_carbs_value)
    TextView carbsValueText;

    @BindView(R.id.textview_carbs_unit)
    TextView carbsUnitText;

    @BindView(R.id.textview_bolus_value)
    TextView bolusValueText;

    @BindView(R.id.textview_hypers_hypos_value)
    TextView hypersHyposValueText;

    @BindView(R.id.imageview_home_profile)
    ImageView profilePictureImageView;

    private Unbinder unbinder;

    private User user;

    public HomeFragment() {
        this.user = UserSettingsManager.getManagedUser();

        this.user.addChangeListener(new RealmChangeListener<User>() {
            @Override
            public void onChange(User element) {
                user = element;
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        this.unbinder = ButterKnife.bind(this, view);
        this.setProfilePhoto();
        this.setDailyValues();

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (this.unbinder != null) {
            this.unbinder.unbind();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        this.setProfilePhoto();
        this.setDailyValues();
    }

    private void setDailyValues() {
        Date today = new Date();
        Date from = DateHelper.dayStarted(today);
        Date to = DateHelper.dayEnded(today);

        this.dateText.setText(getString(R.string.label_today, DateHelper.getFormattedDate(today)));

        this.bloodsugarValueText.setText(getString(R.string.label_average_value, LogManager.getAveragedValue(Column.BLOODSUGAR, from, to)));
        this.carbsValueText.setText(getString(R.string.label_average_value, LogManager.getSummedValue(Column.CARBS, from, to)));
        this.bolusValueText.setText(getString(R.string.label_average_value, LogManager.getSummedValue(Column.BOLUS, from, to)));

        String selectedBloodSugarUnit = getString(BloodsugarUnit.getUnit(this.user.getBloodSugarUnit()).getNameRes());
        String selectedCarbsUnit = getString(CarbsUnit.getUnit(this.user.getCarbsUnit()).getNameRes());
        this.bloodsugarUnitText.setText(getString(R.string.label_bloodsugar_unit_selected, selectedBloodSugarUnit));
        this.carbsUnitText.setText(getString(R.string.label_carbs_unit_selected, selectedCarbsUnit));

        this.hypersHyposValueText.setText(String.format("%1$s / %2$s", LogManager.getHyposCount(), LogManager.getHypersCount()));
    }

    private void setProfilePhoto() {
        if (!PhotoManager.canShowPhoto(getActivity())) {
            return;
        }

        PhotoManager.setPhoto(getActivity(),this.profilePictureImageView, this.user.getProfileImage(), this.user.getPhotoName());
    }
}
