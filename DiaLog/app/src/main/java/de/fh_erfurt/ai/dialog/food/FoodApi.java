package de.fh_erfurt.ai.dialog.food;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.squareup.moshi.Moshi;

import java.io.File;
import java.util.concurrent.TimeUnit;

import de.fh_erfurt.ai.dialog.BuildConfig;
import de.fh_erfurt.ai.dialog.food.model.AuthRequest;
import de.fh_erfurt.ai.dialog.food.model.AuthResponse;
import de.fh_erfurt.ai.dialog.food.model.FoodPageableResponse;
import de.fh_erfurt.ai.dialog.food.model.FoodRequest;
import de.fh_erfurt.ai.dialog.food.model.FoodResponse;
import de.fh_erfurt.ai.dialog.utils.PhotoManager;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Class to handle requests to the food api
 *
 * @author Alexander Zaak
 * @version 1.0
 * @since 06.12.17
 */
public class FoodApi {

    public static final String X_NO_ACCESS_TOKEN = "X-NO-ACCESS-TOKEN";
    public static final String X_IMAGE_HEADER = "X-Image-Header";

    public interface HttpService {
        @GET("food")
        Call<FoodPageableResponse> food(@Query("page") int paging, @Query("limit") int limit, @Query("sort") String sort);

        @GET("food/search")
        Call<FoodPageableResponse> search(@Query(value = "search", encoded = true) String search);

        @GET("food/search")
        Call<FoodPageableResponse> search(@Query(value = "search", encoded = true) String search, @Query("page") int paging, @Query("limit") int limit);

        @GET("food/{id}")
        Call<FoodResponse> getFoodById(@Path("id") String id);

        @POST("food")
        Call<FoodResponse> addFood(@Body FoodRequest food);

        @Headers(X_IMAGE_HEADER + ": true")
        @Multipart
        @POST("food/{id}/image")
        Call<ResponseBody> postImage(@Path("id") String id, @Part("description") RequestBody description, @Part MultipartBody.Part image);

        @Headers(X_NO_ACCESS_TOKEN + ": true")
        @POST("auth")
        Call<AuthResponse> auth(@Body AuthRequest request);
    }

    private static OkHttpClient sOkHttpClient;
    private static HttpService sHttpService;
    private static FoodApiAuthenticator sAuthenticator;

    static HttpService getService(final Context ctx) {
        if (sHttpService == null) {
            synchronized (FoodApi.class) {
                if (sHttpService != null) {
                    return sHttpService;
                }

                final Moshi moshiAdapter = new Moshi.Builder()
                        .add(new DateJsonAdapter())
                        .build();

                final Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(BuildConfig.FOOD_API)
                        .addConverterFactory(MoshiConverterFactory.create(moshiAdapter))
                        .client(getsOkHttpClient(ctx))
                        .build();

                sHttpService = retrofit.create(HttpService.class);
            }
        }

        return sHttpService;
    }

    private static OkHttpClient getsOkHttpClient(final Context ctx) {
        if (sOkHttpClient == null) {
            synchronized (FoodApi.class) {
                if (sOkHttpClient != null) {
                    return sOkHttpClient;
                }

                sOkHttpClient = new OkHttpClient.Builder()
                        .addInterceptor(new FoodApiHttpInterceptor(getAuthenticator(ctx)))
                        .addInterceptor(new HttpLoggingInterceptor()
                                .setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE))
                        .connectTimeout(15, TimeUnit.SECONDS)
                        .readTimeout(15, TimeUnit.SECONDS)
                        .writeTimeout(15, TimeUnit.SECONDS)
                        .build();
            }
        }

        return sOkHttpClient;
    }

    public static OkHttpClient getClient(final Context ctx) {
        return new OkHttpClient.Builder()
                .addInterceptor(new FoodApiHttpInterceptor(getAuthenticator(ctx)))
                .build();
    }

    private static FoodApiAuthenticator getAuthenticator(final Context ctx) {
        if (sAuthenticator == null) {
            synchronized (FoodApi.class) {
                if (sAuthenticator != null) {
                    return sAuthenticator;
                }

                sAuthenticator = new FoodApiAuthenticator(ctx);
            }
        }

        return sAuthenticator;
    }

    /**
     * Method to send an authorize request
     *
     * @param ctx     Context
     * @param request auth request
     * @return auth response
     */
    public static Task<AuthResponse> auth(final Context ctx, AuthRequest request) {
        final TaskCompletionSource<AuthResponse> source = new TaskCompletionSource<>();
        getService(ctx).auth(request).enqueue(new TaskCompletionSourceCallback<AuthResponse>(source));
        return source.getTask();
    }

    /**
     * Method to get pageable food
     *
     * @param ctx   Context
     * @param page  page number
     * @param limit limit
     * @param sort  sort
     * @return pageable food
     */
    public static Task<FoodPageableResponse> getFood(final Context ctx, final int page, final int limit, final String sort) {
        return getAuthenticator(ctx).check().continueWithTask(new Continuation<Void, Task<FoodPageableResponse>>() {
            @Override
            public Task<FoodPageableResponse> then(@NonNull Task<Void> task) throws Exception {
                final TaskCompletionSource<FoodPageableResponse> source = new TaskCompletionSource<>();
                getService(ctx).food(page, limit, sort).enqueue(new TaskCompletionSourceCallback<FoodPageableResponse>(source));
                return source.getTask();
            }
        });
    }

    /**
     * Method to search food by name
     *
     * @param ctx      cotext
     * @param foodName food name
     * @return search result
     */
    public static Task<FoodPageableResponse> search(final Context ctx, final String foodName) {
        return getAuthenticator(ctx).check().continueWithTask(new Continuation<Void, Task<FoodPageableResponse>>() {
            @Override
            public Task<FoodPageableResponse> then(@NonNull Task<Void> task) throws Exception {
                final TaskCompletionSource<FoodPageableResponse> source = new TaskCompletionSource<>();
                getService(ctx).search(foodName).enqueue(new TaskCompletionSourceCallback<FoodPageableResponse>(source));
                return source.getTask();
            }
        });
    }

    /**
     * Method to search food by name
     *
     * @param ctx      Context
     * @param foodName food name
     * @param page     page number
     * @param limit    limit
     * @return pageable food
     */
    public static Task<FoodPageableResponse> search(final Context ctx, final String foodName, final int page, final int limit) {
        return getAuthenticator(ctx).check().continueWithTask(new Continuation<Void, Task<FoodPageableResponse>>() {
            @Override
            public Task<FoodPageableResponse> then(@NonNull Task<Void> task) throws Exception {
                final TaskCompletionSource<FoodPageableResponse> source = new TaskCompletionSource<>();
                getService(ctx).search(foodName, page, limit).enqueue(new TaskCompletionSourceCallback<FoodPageableResponse>(source));
                return source.getTask();
            }
        });
    }

    /**
     * Method to get food by id
     *
     * @param ctx    Context
     * @param foodId food id
     * @return food response
     */
    public static Task<FoodResponse> getFoodById(final Context ctx, final String foodId) {
        return getAuthenticator(ctx).check().continueWithTask(new Continuation<Void, Task<FoodResponse>>() {
            @Override
            public Task<FoodResponse> then(@NonNull Task<Void> task) throws Exception {
                final TaskCompletionSource<FoodResponse> source = new TaskCompletionSource<>();
                getService(ctx).getFoodById(foodId).enqueue(new TaskCompletionSourceCallback<FoodResponse>(source));
                return source.getTask();
            }
        });
    }

    /**
     * Method to add food
     *
     * @param ctx  Context
     * @param food food
     * @return added food
     */
    public static Task<FoodResponse> addFood(final Context ctx, final FoodRequest food) {
        return getAuthenticator(ctx).check().continueWithTask(new Continuation<Void, Task<FoodResponse>>() {
            @Override
            public Task<FoodResponse> then(@NonNull Task<Void> task) throws Exception {
                final TaskCompletionSource<FoodResponse> source = new TaskCompletionSource<>();
                getService(ctx).addFood(food).enqueue(new TaskCompletionSourceCallback<FoodResponse>(source));
                return source.getTask();
            }
        });
    }

    /**
     * Method to post an image
     *
     * @param ctx      Context
     * @param id       food id
     * @param imageUri image uri
     * @return request result
     */
    public static Task<ResponseBody> postImage(final Context ctx, final String id, @NonNull final Uri imageUri) {

        File image = PhotoManager.getCompressedFile(ctx, imageUri);

        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(MediaType.parse(ctx.getContentResolver().getType(imageUri)), image);
        // final RequestBody reqFile = RequestBody.create(MediaType.parse("multipart/form-data"), image);
        final MultipartBody.Part body = MultipartBody.Part.createFormData("foodImage", image.getName(), requestFile);

        // add another part within the multipart request
        String descriptionString = "foodImage";
        final RequestBody description =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, descriptionString);

        return getAuthenticator(ctx).check().continueWithTask(new Continuation<Void, Task<ResponseBody>>() {
            @Override
            public Task<ResponseBody> then(@NonNull Task<Void> task) throws Exception {
                final TaskCompletionSource<ResponseBody> source = new TaskCompletionSource<>();
                getService(ctx).postImage(id, description, body).enqueue(new TaskCompletionSourceCallback<ResponseBody>(source));
                return source.getTask();
            }
        });
    }

    private static final class TaskCompletionSourceCallback<TResult> implements Callback<TResult> {

        private final TaskCompletionSource<TResult> source;

        TaskCompletionSourceCallback(final TaskCompletionSource<TResult> source) {
            this.source = source;
        }

        @Override
        public void onResponse(final Call<TResult> call, final retrofit2.Response<TResult> response) {
            if (response.isSuccessful()) {
                source.setResult(response.body());
            } else {
                source.setException(new Exception("Expected successful response, got: " + response.code()));
            }
        }

        @Override
        public void onFailure(final Call<TResult> call, final Throwable th) {
            source.setException(new Exception(th));
        }
    }
}