package de.fh_erfurt.ai.dialog.utils;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.View;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;

import de.fh_erfurt.ai.dialog.BuildConfig;
import de.fh_erfurt.ai.dialog.LoginActivity;

/**
 * Utility class to handling authorization by google
 *
 * @author Alexander Zaak
 * @version 1.0
 * @since 15.11.17
 */
public abstract class GoogleAuth implements GoogleApiClient.OnConnectionFailedListener {

    private static final String SERVER_CLIENT_ID = BuildConfig.SERVER_CLIENT_ID;
    private static final int REQUEST_CODE_SIGN_IN = 2211;
    private GoogleApiClient mGoogleApiClient;

    protected GoogleAuth(final SignInButton btnSignIn, final LoginActivity loginActivity) {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken(SERVER_CLIENT_ID)
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(loginActivity)
                .enableAutoManage(loginActivity, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                loginActivity.startActivityForResult(signInIntent, REQUEST_CODE_SIGN_IN);
            }
        });
    }

    /**
     * Notify this class about the {@link LoginActivity#onResume()} event.
     */
    public final void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_SIGN_IN) {
            handleSignInResult(Auth.GoogleSignInApi.getSignInResultFromIntent(data));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (!connectionResult.hasResolution()) {
            onError("Connection failed and has no resolution. code:" + connectionResult.getErrorCode());
        }
    }

    /**
     * Called once we obtain a token from Google Sign In API.
     *
     * @param result contains the token obtained from Google Sign In API.
     */
    public abstract void onRegistrationComplete(final GoogleSignInResult result);

    /**
     * Called in case of authentication or other errors.
     * <p>
     * Adapter method, developer might want to override this method  to provide
     * custom logic.
     */
    public abstract void onError(String error);

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            onRegistrationComplete(result);
        } else {
            onError("Error on sign in status: "+ result.getStatus());
        }
    }
}
