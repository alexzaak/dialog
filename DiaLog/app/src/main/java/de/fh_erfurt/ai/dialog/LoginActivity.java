package de.fh_erfurt.ai.dialog;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.fh_erfurt.ai.dialog.controller.UserManager;
import de.fh_erfurt.ai.dialog.food.FoodApi;
import de.fh_erfurt.ai.dialog.food.model.AuthRequest;
import de.fh_erfurt.ai.dialog.food.model.AuthResponse;
import de.fh_erfurt.ai.dialog.utils.GoogleAuth;
import de.fh_erfurt.ai.dialog.utils.PrefUtils;
import io.realm.ObjectServerError;
import io.realm.SyncCredentials;
import io.realm.SyncUser;
import timber.log.Timber;

import static de.fh_erfurt.ai.dialog.BuildConfig.AUTH_URL;

/**
 * LoginActivity to sign in by google auth
 *
 * @author Alexander Zaak
 * @version 1.0
 * @since 11.12.17
 */
public class LoginActivity extends AppCompatActivity implements SyncUser.Callback<SyncUser> {

    private GoogleAuth googleAuth;
    private PrefUtils prefUtils;

    @BindView(R.id.google_sign_in)
    SignInButton googleAuthBtn;

    @BindView(R.id.progressbar)
    View progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        this.googleAuthBtn.setSize(SignInButton.SIZE_STANDARD);
        this.prefUtils = new PrefUtils(this);

        this.googleAuth = new GoogleAuth(googleAuthBtn, this) {
            @Override
            public void onRegistrationComplete(GoogleSignInResult result) {
                login(result.getSignInAccount());
            }

            @Override
            public void onError(String error) {
                // Todo: Error handling
                progressBar.setVisibility(View.GONE);
                Timber.e(error);
            }
        };
    }

    @Override
    protected void onStart() {
        super.onStart();
        login(GoogleSignIn.getLastSignedInAccount(this));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        this.googleAuth.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onSuccess(@NonNull SyncUser result) {
        loginComplete(result);
    }

    @Override
    public void onError(@NonNull ObjectServerError error) {
        //TODO: handle error
        this.progressBar.setVisibility(View.GONE);
        Timber.e(error.getException());
    }

    private void login(GoogleSignInAccount account) {
        if (account == null || account.getIdToken() == null) {
            this.progressBar.setVisibility(View.GONE);
            Timber.e("unable to sign in with google. Account was null.");
            return;
        }

        this.progressBar.setVisibility(View.VISIBLE);
        SyncCredentials credentials = SyncCredentials.google(account.getIdToken());
        this.prefUtils.setProvider(credentials.getIdentityProvider());
        this.prefUtils.setGoogleToken(credentials.getUserIdentifier());

        SyncUser.loginAsync(credentials, AUTH_URL, LoginActivity.this);
    }

    private void loginComplete(final SyncUser user) {
        UserManager.signIn(user, new UserManager.Callback() {
            @Override
            public void done() {
                FoodApi.auth(LoginActivity.this, getAuthData()).addOnCompleteListener(new OnCompleteListener<AuthResponse>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResponse> task) {
                        progressBar.setVisibility(View.GONE);
                        if (task.isSuccessful()) {
                            prefUtils.setAccessToken(task.getResult().refreshToken.token);
                            prefUtils.setExpireAt(task.getResult().refreshToken.tokenData.expires);

                            startActivity(MainActivity.newInstance(LoginActivity.this));
                            finish();
                        }
                    }
                });
            }
        });
    }

    private AuthRequest getAuthData() {
        AuthRequest request = new AuthRequest();
        request.setData(this.prefUtils.getGoogleToken());
        request.setProvider(this.prefUtils.getProvider());
        return request;
    }
}
