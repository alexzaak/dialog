package de.fh_erfurt.ai.dialog.food.model;

import com.squareup.moshi.Json;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexander on 07.01.18.
 */

public class AuthResponse {

    @Json(name = "refresh_token")
    public RefreshToken refreshToken;

    @Override
    public String toString() {
        return "AuthResponse{" +
                "token='" + refreshToken.token + '\'' +
                ", expires=" + refreshToken.tokenData.expires +
                '}';
    }

    public static final class RefreshToken {
        public String token;

        @Json(name = "token_data")
        public TokenData tokenData;


        public static final class TokenData {
            @Json(name = "app_id")
            public String appId;
            public String identity;

            @Json(name = "access")
            public List<String> accessList = new ArrayList<>();
            public String salt;
            public long expires;

            @Json(name = "is_admin")
            public boolean isAdmin;
        }
    }
}
