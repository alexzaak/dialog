package de.fh_erfurt.ai.dialog.food.model;

/**
 * Created by alexander on 07.01.18.
 */

public class AuthRequest {
    private String provider;
    private String data;

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
