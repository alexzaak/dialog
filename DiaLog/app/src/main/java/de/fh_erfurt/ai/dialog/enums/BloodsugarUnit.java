package de.fh_erfurt.ai.dialog.enums;

import android.support.annotation.IdRes;
import android.support.annotation.StringRes;

import de.fh_erfurt.ai.dialog.R;

/**
 * Enumeration for Blood sugar unit
 *
 * @author Max Edenharter
 * @version 1.0
 * @since 10.05.17
 */
public enum BloodsugarUnit {

    /* Unit milligrams per deciliter */
    MG_DL(1, R.id.radio_bloodsugar_unit_mg_dl, R.string.label_bloodsugar_unit_mg_dl),

    /* Unit millimol per liter */
    MMOL_L(2, R.id.radio_bloodsugar_unit_mmol_l, R.string.label_bloodsugar_unit_mmol_l);

    private final int id;
    private final int radioRes;
    private final int nameRes;

    BloodsugarUnit(int id, @IdRes int radioRes, @StringRes int nameRes) {
        this.id = id;
        this.radioRes = radioRes;
        this.nameRes = nameRes;
    }

    /**
     * Method to get Unit ID
     *
     * @return unit ID
     */
    public int getId() {
        return this.id;
    }

    /**
     * Method to get the corresponding radio button
     *
     * @return radio button ID {@link R.id}
     */
    public int getRadioRes() {
        return this.radioRes;
    }

    /**
     * Method to get Unit Name
     *
     * @return String resource {@link R.string}
     */
    public int getNameRes() {
        return this.nameRes;
    }

    /**
     * Method to get Blood sugar unit by given ID
     *
     * @param unit unit ID
     * @return {@link BloodsugarUnit}
     */
    public static BloodsugarUnit getUnit(int unit) {
        switch (unit) {
            case 1:
                return MG_DL;
            case 2:
                return MMOL_L;
            default:
                return MG_DL;
        }
    }

    /**
     * Method to get Unit ID by given Radio Button ID
     *
     * @param checkedRadioButtonId Radio Button ID {@link R.id}
     * @return Blood sugar Unit ID
     */
    public static int getUnitId(@IdRes int checkedRadioButtonId) {
        switch (checkedRadioButtonId) {
            case R.id.radio_bloodsugar_unit_mg_dl:
                return MG_DL.getId();
            case R.id.radio_bloodsugar_unit_mmol_l:
                return MMOL_L.getId();
            default:
                return MG_DL.getId();
        }
    }
}
