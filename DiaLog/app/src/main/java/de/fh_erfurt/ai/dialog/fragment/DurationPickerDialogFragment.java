package de.fh_erfurt.ai.dialog.fragment;

import mobi.upod.timedurationpicker.TimeDurationPicker;
import mobi.upod.timedurationpicker.TimeDurationPickerDialogFragment;

/**
 * Shows a time duration picker.
 */
public class DurationPickerDialogFragment extends TimeDurationPickerDialogFragment {
    public interface InteractionDurationPicker {
        void onDurationSet(long duration);
    }

    private InteractionDurationPicker listener;

    @Override
    protected long getInitialDuration() {
        return 60 * 60 * 1000;
    }

    @Override
    protected int setTimeUnits() {
        return TimeDurationPicker.HH_MM;
    }

    @Override
    public void onDurationSet(TimeDurationPicker view, long duration) {
        this.listener.onDurationSet(duration);
    }

    public void setOnDurationSetListener(InteractionDurationPicker listener) {
        this.listener = listener;
    }
}
