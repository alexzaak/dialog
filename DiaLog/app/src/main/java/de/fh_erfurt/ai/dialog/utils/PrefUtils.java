package de.fh_erfurt.ai.dialog.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Utility class to store settings into shared preferences.
 *
 * @author Alexander Zaak
 * @version 1.0
 * @since 07.01.18
 */
public class PrefUtils {

    private static final String PREF_GOOGLE_TOKEN = "pref-google-token";
    private static final String PREF_ACCESS_TOKEN = "pref-access-token";
    private static final String PREF_EXPIRE_AT = "pref-expire-at";
    private static final String PREF_PROVIDER = "pref-provider";


    private SharedPreferences prefs;

    public PrefUtils(final Context ctx) {
        this.prefs = ctx.getSharedPreferences(ctx.getPackageName(), Context.MODE_PRIVATE);
    }

    public void setGoogleToken(final String token) {
        this.prefs.edit().putString(PREF_GOOGLE_TOKEN, token).apply();
    }

    public String getGoogleToken() {
        return this.prefs.getString(PREF_GOOGLE_TOKEN, null);
    }

    public void setAccessToken(final String token) {
        this.prefs.edit().putString(PREF_ACCESS_TOKEN, token).apply();
    }

    public String getAccessToken() {
        return this.prefs.getString(PREF_ACCESS_TOKEN, null);
    }

    public void setExpireAt(final long expireAt) {
        this.prefs.edit().putLong(PREF_EXPIRE_AT, expireAt).apply();
    }

    public long getExpireAt() {
        return this.prefs.getLong(PREF_EXPIRE_AT, 0);
    }

    public void setProvider(String provider) {
        this.prefs.edit().putString(PREF_PROVIDER, provider).apply();
    }

    public String getProvider() {
        return this.prefs.getString(PREF_PROVIDER, null);
    }
}
