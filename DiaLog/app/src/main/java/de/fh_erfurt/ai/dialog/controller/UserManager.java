package de.fh_erfurt.ai.dialog.controller;

import de.fh_erfurt.ai.dialog.BuildConfig;
import io.realm.Realm;
import io.realm.SyncConfiguration;
import io.realm.SyncUser;

/**
 * Manager to sign in or logout user
 *
 * @author Alexander Zaak
 * @version 1.0
 * @since 22.11.17
 */
public class UserManager {

    public interface Callback {
        void done();
    }

    /**
     * Method to Sign in user and sync the DB
     *
     * @param user SyncUser
     * @param cb   Callback
     */
    public static void signIn(final SyncUser user, final Callback cb) {
        SyncConfiguration defaultConfig = new SyncConfiguration.Builder(user, BuildConfig.SYNC_URL)
                .waitForInitialRemoteData()
                .build();

        Realm.setDefaultConfiguration(defaultConfig);
        Realm.getInstanceAsync(defaultConfig, new Realm.Callback() {
            @Override
            public void onSuccess(Realm realm) {
                cb.done();
            }
        });
    }

    /**
     * Logout user
     */
    public static void logout() {
        SyncUser.currentUser().logout();
    }
}
