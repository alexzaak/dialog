package de.fh_erfurt.ai.dialog.food;

import com.squareup.moshi.FromJson;
import com.squareup.moshi.JsonDataException;
import com.squareup.moshi.JsonReader;
import com.squareup.moshi.ToJson;

import java.util.Date;

import de.fh_erfurt.ai.dialog.utils.DateHelper;

/**
 * Json adapter to handle Date instances in model classes
 *
 * @author Alexander Zaak
 * @version 1.0
 * @since 07.01.18
 */
public final class DateJsonAdapter {

    /**
     * Method to convert Date to unix time stampe
     *
     * @param dt Date
     * @return unix time stamp
     */
    @ToJson
    public long toJson(final Date dt) {
        return DateHelper.toUnixTime(dt);
    }

    /**
     * Method to convert json value to Date
     *
     * @param reader json value
     * @return Date
     */
    @FromJson
    public Date fromJson(final JsonReader reader) {
        try {
            if (reader.peek() == JsonReader.Token.NUMBER) {
                return DateHelper.fromUnixTime(reader.nextLong());
            } else if (reader.peek() == JsonReader.Token.STRING) {
                return DateHelper.parse(reader.nextString());
            } else {
                throw new JsonDataException("Unable to read token: " + reader.peek());
            }
        } catch (final Throwable th) {
            throw new JsonDataException(th);
        }
    }
}
