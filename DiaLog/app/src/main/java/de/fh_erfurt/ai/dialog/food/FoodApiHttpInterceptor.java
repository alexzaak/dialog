package de.fh_erfurt.ai.dialog.food;

import android.support.annotation.NonNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import static de.fh_erfurt.ai.dialog.food.FoodApi.X_IMAGE_HEADER;

/**
 * @author Alexander Zaak
 * @version 1.0
 * @since 06.01.18
 */
public class FoodApiHttpInterceptor implements Interceptor {

    private static final String HTTP_HEADER_ACCEPT = "Accept";
    private static final String HTTP_HEADER_CONTENT_TYPE = "Content-Type";

    private final FoodApiAuthenticator authenticator;

    public FoodApiHttpInterceptor(final FoodApiAuthenticator authenticator) {
        this.authenticator = authenticator;
    }

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        final Request.Builder builder = chain.request().newBuilder();

        if (chain.request().header(FoodApi.X_NO_ACCESS_TOKEN) == null) {
            this.authenticator.authenticate(builder);
        }

        if (chain.request().header(X_IMAGE_HEADER) == null) {
            // set defaults
            builder.addHeader(HTTP_HEADER_ACCEPT, "application/json");
            builder.addHeader(HTTP_HEADER_CONTENT_TYPE, "application/json");
        } else {
            builder.addHeader(HTTP_HEADER_ACCEPT, "image/jpeg");
            builder.addHeader(HTTP_HEADER_CONTENT_TYPE, "image/jpeg");
        }

        return chain.proceed(builder.build());
    }
}
