package de.fh_erfurt.ai.dialog.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.mobsandgeeks.saripaar.annotation.DecimalMax;
import com.mobsandgeeks.saripaar.annotation.DecimalMin;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.fh_erfurt.ai.dialog.CreateLogEntryActivity;
import de.fh_erfurt.ai.dialog.FoodActivity;
import de.fh_erfurt.ai.dialog.R;
import de.fh_erfurt.ai.dialog.controller.LogManager;
import de.fh_erfurt.ai.dialog.controller.UserSettingsManager;
import de.fh_erfurt.ai.dialog.enums.BloodsugarUnit;
import de.fh_erfurt.ai.dialog.enums.CarbsUnit;
import de.fh_erfurt.ai.dialog.model.LogEntry;
import de.fh_erfurt.ai.dialog.model.User;
import de.fh_erfurt.ai.dialog.utils.BolusCalc;
import de.fh_erfurt.ai.dialog.utils.DiaConverter;
import de.fh_erfurt.ai.dialog.utils.NumberHelper;
import de.fh_erfurt.ai.dialog.utils.ViewUtils;
import mobi.upod.timedurationpicker.TimeDurationUtil;

/**
 * Shows the form to create new log entries or edit one.
 *
 * @author Max Edenharter
 */
public class CreateLogFragment extends Fragment implements ValidationListener, DurationPickerDialogFragment.InteractionDurationPicker, TextWatcher {

    public static final String SAVE_STATE_BLOOD_SUGAR = "save_state_blood_sugar";
    public static final String SAVE_STATE_BOLUS = "save_state_blood_bolus";
    public static final String SAVE_STATE_CORRECTION = "save_state_blood_correction";
    public static final String SAVE_STATE_NOTICE = "save_state_blood_notice";


    @NotEmpty(messageResId = R.string.validation_msg_required_field)
    @DecimalMin(value = 0.f, messageResId = R.string.validation_msg_greater_zero)
    @DecimalMax(value = 1000.f, messageResId = R.string.validation_msg_less_50)
    @BindView(R.id.input_bloodsugar)
    TextInputEditText bloodsugarInput;

    @BindView(R.id.textview_bloodsugar_unit)
    TextView bloodsugarUnitTextView;

    @NotEmpty(messageResId = R.string.validation_msg_required_field)
    @DecimalMin(value = 0.f, messageResId = R.string.validation_msg_greater_zero)
    @DecimalMax(value = 10000.f, messageResId = R.string.validation_msg_less_10000)
    @BindView(R.id.input_carbs)
    TextInputEditText carbsInput;

    @BindView(R.id.textview_carbs_unit)
    TextView carbsUnitTextView;

    @BindView(R.id.textview_boluscalc_bolus)
    TextView bolusCalcBolus;
    @BindView(R.id.textview_boluscalc_corr)
    TextView bolusCalcCorr;
    @BindView(R.id.button_apply_boluscalc)
    Button applyBolusCalcButton;

    @NotEmpty(messageResId = R.string.validation_msg_required_field)
    @DecimalMin(value = 0.f, messageResId = R.string.validation_msg_greater_zero)
    @DecimalMax(value = 100.f, messageResId = R.string.validation_msg_less_hundred)
    @BindView(R.id.input_bolus)
    TextInputEditText bolusInput;

    @BindView(R.id.input_correction)
    TextInputEditText correctionInput;

    @BindView(R.id.textview_activity)
    TextView activityTextView;

    @BindView(R.id.button_pick_activity)
    Button pickActivityButton;

    @BindView(R.id.input_notes)
    TextInputEditText notesInput;

    @BindView(R.id.btn_search_food)
    ImageButton searchFood;

    private User userSettings;
    private Unbinder unbinder;
    private Validator validator;
    private BolusCalc bolusCalc;
    private LogEntry logEntry;
    private double calcResultBolus;
    private double calcResultCorr;

    private DurationPickerDialogFragment durationPickerFragment;
    private NumberHelper numberHelper;

    public CreateLogFragment() {
        this.userSettings = UserSettingsManager.getUser();
        this.bolusCalc = new BolusCalc(this.userSettings);

        this.durationPickerFragment = new DurationPickerDialogFragment();
        this.durationPickerFragment.setOnDurationSetListener(this);

        numberHelper = new NumberHelper();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        View view = inflater.inflate(R.layout.fragment_create_log, container, false);

        unbinder = ButterKnife.bind(this, view);

        validator = new Validator(this);
        validator.setValidationListener(this);

        this.bloodsugarUnitTextView.setText(getString(BloodsugarUnit.getUnit(userSettings.getBloodSugarUnit()).getNameRes()));
        this.carbsUnitTextView.setText(getString(CarbsUnit.getUnit(userSettings.getCarbsUnit()).getNameRes()));

        this.bloodsugarInput.addTextChangedListener(this);
        this.carbsInput.addTextChangedListener(this);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String logEntryId = bundle.getString(CreateLogEntryActivity.EXTRA_LOG_ENTRY_ID);
            this.logEntry = LogManager.getLogEntry(logEntryId);
            setLogEntryForm();
        } else {
            this.logEntry = new LogEntry();
        }

        this.applyBolusCalcButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bolusInput.setText(numberHelper.format(calcResultBolus));
                correctionInput.setText(numberHelper.format(calcResultCorr));
                ViewUtils.hideKeyboard(getActivity(), v);
            }
        });

        this.pickActivityButton
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        durationPickerFragment.show(getActivity().getFragmentManager(), "timeDurationDialog");
                    }
                });

        this.searchFood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(FoodActivity.newInstance(getActivity()), CreateLogEntryActivity.REQUEST_CODE);
            }
        });

        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState != null) {
            this.bloodsugarInput.setText(String.valueOf(savedInstanceState.getDouble(SAVE_STATE_BLOOD_SUGAR)));
            this.bolusInput.setText(String.valueOf(savedInstanceState.getDouble(SAVE_STATE_BOLUS)));
            this.correctionInput.setText(String.valueOf(savedInstanceState.getDouble(SAVE_STATE_CORRECTION)));
            this.notesInput.setText(savedInstanceState.getString(SAVE_STATE_NOTICE));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putDouble(SAVE_STATE_BLOOD_SUGAR, NumberHelper.getDoubleValue(this.bloodsugarInput));
        outState.putDouble(SAVE_STATE_BOLUS, NumberHelper.getDoubleValue(this.bolusInput));
        outState.putDouble(SAVE_STATE_CORRECTION, NumberHelper.getDoubleValue(this.correctionInput));
        outState.putString(SAVE_STATE_NOTICE, this.notesInput.getText().toString());
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) {
            unbinder.unbind();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_create_log, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_save) {
            validator.validate();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onValidationSucceeded() {
        this.logEntry.setBloodSugar(NumberHelper.getDoubleValue(this.bloodsugarInput));
        this.logEntry.setCarbs(NumberHelper.getDoubleValue(this.carbsInput));
        this.logEntry.setBolus(NumberHelper.getDoubleValue(this.bolusInput));
        this.logEntry.setCorrection(NumberHelper.getDoubleValue(this.correctionInput));
        this.logEntry.setNotes(this.notesInput.getText().toString());

        LogManager.saveLogEntry(this.logEntry);
        Toast.makeText(getContext(), getString(R.string.log_entry_saved), Toast.LENGTH_SHORT).show();
        getActivity().finish();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getContext());

            // Display error messages ;)
            if (view instanceof TextInputEditText) {
                ((TextInputEditText) view).setError(message);
            } else {
                if (getView() != null) {
                    Snackbar.make(getView(), message, Snackbar.LENGTH_LONG).show();
                }
            }
        }
    }

    @Override
    public void onDurationSet(long duration) {
        this.logEntry.setActivity(duration);
        String durationString = String.format(Locale.getDefault(), "%02d:%02d", TimeDurationUtil.hoursOf(duration), TimeDurationUtil.minutesInHourOf(duration));
        this.activityTextView.setText(durationString);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        double bloodSugar = NumberHelper.getDoubleValue(bloodsugarInput);
        double bolus = bolusCalc.calcBolus(bloodSugar, NumberHelper.getDoubleValue(carbsInput));
        double correction = bolusCalc.calcCorrection(bloodSugar);

        this.calcResultBolus = bolus;
        this.calcResultCorr = correction;

        bolusCalcBolus.setText(getString(R.string.label_average_value, bolus));
        bolusCalcCorr.setText(getString(R.string.label_average_value, correction));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            final double result = data.getDoubleExtra(FoodActivity.EXTRA_CARBS, 0.f);
            final CarbsUnit currentCarbsUnit = CarbsUnit.getUnit(this.userSettings.getCarbsUnit());
            final double convertedResult = DiaConverter.convertCarbs(CarbsUnit.CU, currentCarbsUnit, result);
            this.carbsInput.setText(numberHelper.format(convertedResult));
        }
    }

    /**
     * Fills the log entry form with values of the current log entry.
     */
    private void setLogEntryForm() {
        this.bloodsugarInput.setText(numberHelper.format(this.logEntry.getBloodSugar()));
        this.carbsInput.setText(numberHelper.format(this.logEntry.getCarbs()));
        this.bolusInput.setText(numberHelper.format(this.logEntry.getBolus()));
        this.correctionInput.setText(numberHelper.format(this.logEntry.getCorrection()));
        this.onDurationSet(this.logEntry.getActivity());
        this.notesInput.setText(this.logEntry.getNotes());
    }

    public static CreateLogFragment create(final String logEntryId) {
        CreateLogFragment fragment = new CreateLogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(CreateLogEntryActivity.EXTRA_LOG_ENTRY_ID, logEntryId);
        fragment.setArguments(bundle);
        return fragment;
    }
}
