package de.fh_erfurt.ai.dialog;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.fh_erfurt.ai.dialog.adapter.ViewPagerAdapter;
import de.fh_erfurt.ai.dialog.fragment.HomeFragment;
import de.fh_erfurt.ai.dialog.fragment.LogFragment;
import de.fh_erfurt.ai.dialog.fragment.SettingsFragment;

/**
 * MainActivity which implements the ViewPager and show a Tabbed view
 *
 * @author Alexander Zaak
 * @version 1.0
 * @since 03.05.17
 */
public class MainActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tabs)
    TabLayout tabLayout;

    @BindView(R.id.viewpager)
    ViewPager viewPager;

    @BindView(R.id.fab)
    FloatingActionButton fab;

    private ViewPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new HomeFragment(), getString(R.string.tab_home));
        adapter.addFragment(new LogFragment(), getString(R.string.tab_log));
        adapter.addFragment(new SettingsFragment(), getString(R.string.tab_settings));

        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(this);
    }

    @OnClick(R.id.fab)
    void onActionButtonClick() {
        startActivity(CreateLogEntryActivity.newIntent(this));
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (adapter.getPageTitle(position).equals(getString(R.string.tab_settings))) {
            fab.setVisibility(View.GONE);
        } else {
            fab.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public static Intent newInstance(Context ctx) {
        return new Intent(ctx, MainActivity.class);
    }
}
