package de.fh_erfurt.ai.dialog;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import de.fh_erfurt.ai.dialog.fragment.CreateLogFragment;

/**
 * CreateLogEntryActivity to create a new Log-Entry
 *
 * @author Max Edenharter
 * @version 1.0
 * @since 03.05.17
 */
public class CreateLogEntryActivity extends AppCompatActivity {

    public static final String EXTRA_LOG_ENTRY_ID = "EXTRA_LOG_ENTRY_ID";

    public static final int REQUEST_CODE = 601;

    private static final String CREATE_LOG_FRAGMENT_TAG = "create_log_fragment_tag";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_log_entry);

        // Check that the activity is using the layout version with
        // the fragment_container FrameLayout
        if (findViewById(R.id.content) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }
            // Create a new Fragment to be placed in the activity layout
            final String logEntryId = getIntent().getStringExtra(EXTRA_LOG_ENTRY_ID);
            Fragment fragment;
            if (logEntryId != null) {
                fragment = CreateLogFragment.create(logEntryId);
            } else {
                fragment = new CreateLogFragment();
            }

            // Add the fragment to the 'fragment_container' FrameLayout
            getSupportFragmentManager().beginTransaction().replace(R.id.content, fragment, CREATE_LOG_FRAGMENT_TAG).commit();
        }
    }

    public static Intent newIntent(Context ctx, String id) {
        Intent intent = new Intent(ctx, CreateLogEntryActivity.class);
        intent.putExtra(EXTRA_LOG_ENTRY_ID, id);
        return intent;
    }

    public static Intent newIntent(Context ctx) {
        return new Intent(ctx, CreateLogEntryActivity.class);
    }
}
