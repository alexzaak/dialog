package de.fh_erfurt.ai.dialog.controller;

import java.util.UUID;

import de.fh_erfurt.ai.dialog.BuildConfig;
import de.fh_erfurt.ai.dialog.model.User;
import io.realm.Realm;
import io.realm.SyncConfiguration;
import io.realm.SyncUser;

/**
 * UserSettingsManager
 * Database Manager for user settings
 *
 * @author Alexander Zaak
 * @version 1.0
 * @since 17.05.17
 */
public class UserSettingsManager {

    /**
     * Method to create a new user. The user id will be generated automatically
     *
     * @return created user
     */
    public static User createUser() {
        Realm realm = Realm.getDefaultInstance();

        String userId = UUID.randomUUID().toString();

        realm.beginTransaction();
        User user = realm.createObject(User.class, userId);
        realm.commitTransaction();
        realm.close();

        return user;
    }

    /**
     * Method to get a managed user.
     *
     * @return managed user
     */
    public static User getManagedUser() {
        Realm realm = Realm.getDefaultInstance();
        User user = realm.where(User.class).findFirst();

        if (user == null || !user.isValid()) {
            user = createUser();
            return user;
        }
        return user;
    }

    /**
     * Method to get a simple user.
     *
     * @return simple user
     */
    public static User getUser() {
        Realm realm = Realm.getDefaultInstance();
        User user = realm.copyFromRealm(getManagedUser());
        realm.close();
        return user;
    }

    /**
     * Method to save settings for the given user
     *
     * @param user user to save
     */
    public static void saveUserSettings(User user) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(user);
        realm.commitTransaction();
        realm.close();
    }

    /**
     * Method to get the threshold value for hyper.
     *
     * @return threshold for hyper
     */
    public static double getThresholdHyper() {
        return getUser().getThresholdHyper();
    }

    /**
     * Method to get the threshold value for hypo.
     *
     * @return threshold for hypo
     */
    public static double getThresholdHypo() {
        return getUser().getThresholdHypo();
    }


    /**
     * Method to enable sync for user settings
     */
    public static void syncUserSettings(SyncUser user) {

        SyncConfiguration config = new SyncConfiguration.Builder(user, BuildConfig.SYNC_URL).initialData(new Realm.Transaction() {

            @Override
            public void execute(Realm realm) {
                // TODO: Stuff
            }
        }).build();
    }
}
