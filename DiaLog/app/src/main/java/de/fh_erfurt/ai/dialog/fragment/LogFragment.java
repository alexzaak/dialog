package de.fh_erfurt.ai.dialog.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.fh_erfurt.ai.dialog.R;
import de.fh_erfurt.ai.dialog.adapter.LogAdapter;

/**
 * Shows the log.
 */
public class LogFragment extends Fragment {

    @BindView(R.id.list)
    RecyclerView recyclerView;

    private LogAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private Unbinder unbinder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_log_list, container, false);
        this.unbinder = ButterKnife.bind(this, view);

        this.recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        this.layoutManager = new LinearLayoutManager(getContext());
        this.recyclerView.setLayoutManager(this.layoutManager);

        this.adapter = new LogAdapter(getContext());
        this.recyclerView.setAdapter(adapter);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (this.unbinder != null) {
            this.unbinder.unbind();
        }
    }
}
