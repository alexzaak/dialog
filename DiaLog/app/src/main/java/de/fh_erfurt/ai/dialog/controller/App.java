package de.fh_erfurt.ai.dialog.controller;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.LruCache;
import com.squareup.picasso.Picasso;

import net.danlew.android.joda.JodaTimeAndroid;

import de.fh_erfurt.ai.dialog.BuildConfig;
import de.fh_erfurt.ai.dialog.food.FoodApi;
import io.realm.Realm;
import io.realm.log.RealmLog;
import timber.log.Timber;

/**
 * @author Max Edenharter
 * @version 1.0
 * @since 03.05.17
 */
public class App extends Application {
    private static App mInstance;
    private static Context context;

    @Override
    public void onCreate() {
        mInstance = this;
        context = super.getApplicationContext();

        JodaTimeAndroid.init(this);
        this.initDatabase();

        Picasso.Builder builder = new Picasso.Builder(this);
        builder.downloader(new OkHttp3Downloader(FoodApi.getClient(this)));
        builder.memoryCache(new LruCache(this));
        Picasso built = builder.build();
        built.setIndicatorsEnabled(true);
        if (BuildConfig.DEBUG) {
            built.setLoggingEnabled(true);
        }
        Picasso.setSingletonInstance(built);

        super.onCreate();
    }

    public static synchronized App getInstance() {
        return mInstance;
    }

    public static Context getContext() {
        return context;
    }

    private void initDatabase() {
        Realm.init(this);
        if (BuildConfig.DEBUG) {
            RealmLog.setLevel(Log.DEBUG);
            Timber.plant(new Timber.DebugTree());
        }
    }
}
