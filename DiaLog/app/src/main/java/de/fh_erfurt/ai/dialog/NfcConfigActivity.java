package de.fh_erfurt.ai.dialog;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Activity for NFC-Tag configuration
 *
 * @author Alexander Zaak
 * @version 1.0
 * @since 20.07.17
 */
public class NfcConfigActivity extends AppCompatActivity {

    @BindView(R.id.textview_message)
    TextView mNfcMessage;

    @BindView(R.id.progress)
    ProgressBar mProgress;

    @BindView(R.id.nfc_disabled_container)
    LinearLayout nfcDisabledContainer;

    @BindView(R.id.nfc_enabled_container)
    LinearLayout nfcEnabledContainer;

    @BindView(R.id.button_nfc_settings)
    Button nfcSettingButton;

    @BindView(R.id.imageview_logo)
    ImageView nfcLogoImageView;

    private Unbinder unbinder;

    private NfcAdapter mNfcAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);
        this.unbinder = ButterKnife.bind(this);

        this.mNfcAdapter = NfcAdapter.getDefaultAdapter(this);

        if (this.mNfcAdapter == null) {
            Toast.makeText(this, getString(R.string.message_nfc_not_available), Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        checkNfcSettings();

        this.nfcSettingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(android.provider.Settings.ACTION_NFC_SETTINGS));
            }
        });

        handleIntent(getIntent());
    }

    /**
     * Unbinds Butterknife
     */
    @Override
    protected void onDestroy() {
        if (this.unbinder != null) {
            this.unbinder.unbind();
        }
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkNfcSettings();
        enableForegroundDispatch();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (this.mNfcAdapter != null) {
            this.mNfcAdapter.disableForegroundDispatch(this);
        }
    }

    /**
     * This method gets called, when a new Intent gets associated with the current activity instance.
     * Instead of creating a new activity, onNewIntent will be called. For more information have a look
     * at the documentation.
     * <p>
     * In our case this method gets called, when the user attaches a Tag to the device.
     */
    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    /**
     * Method to enable foreground dispatching to detect NFC-Tags
     */
    private void enableForegroundDispatch() {
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        IntentFilter ndefDetected = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
        IntentFilter techDetected = new IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED);
        IntentFilter[] nfcIntentFilter = new IntentFilter[]{techDetected, tagDetected, ndefDetected};

        PendingIntent pendingIntent = PendingIntent.getActivity(
                this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

        if (this.mNfcAdapter != null) {
            this.mNfcAdapter.enableForegroundDispatch(this, pendingIntent, nfcIntentFilter, null);
        }
    }

    /**
     * Method to check if NFC is enabled
     */
    private void checkNfcSettings() {
        if (!this.mNfcAdapter.isEnabled()) {
            this.nfcDisabledContainer.setVisibility(View.VISIBLE);
            this.nfcEnabledContainer.setVisibility(View.GONE);
        } else {
            this.nfcDisabledContainer.setVisibility(View.GONE);
            this.nfcEnabledContainer.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Method to handle a new intent
     *
     * @param intent detected intent
     */
    private void handleIntent(Intent intent) {
        Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);

        toggleNfcLogo(false);
        if (tag != null) {
            Toast.makeText(this, getString(R.string.message_nfc_detected), Toast.LENGTH_LONG).show();

            Ndef ndef = Ndef.get(tag);
            this.mProgress.setVisibility(View.VISIBLE);
            toggleNfcLogo(true);
            writeToNfc(ndef);
        }
    }

    /**
     * Method to write to NFC-Tag
     *
     * @param ndef detected {@link Ndef}
     */
    private void writeToNfc(Ndef ndef) {
        this.mNfcMessage.setText(getString(R.string.message_write_progress));
        if (ndef != null) {
            try {
                ndef.connect();
                NdefRecord mimeRecord = NdefRecord.createApplicationRecord(getPackageName());
                ndef.writeNdefMessage(new NdefMessage(mimeRecord));
                ndef.close();
                //Write Successful
                this.mNfcMessage.setText(getString(R.string.message_write_success));

            } catch (IOException | FormatException e) {
                this.mNfcMessage.setText(getString(R.string.message_write_error));
                toggleNfcLogo(false);
            } finally {
                this.mProgress.setVisibility(View.GONE);
            }
        }
    }

    /**
     * Method to toggle NFC-Logo in the view
     *
     * @param isConnected connection state
     */
    private void toggleNfcLogo(boolean isConnected) {
        if (isConnected) {
            this.nfcLogoImageView.setColorFilter(ContextCompat.getColor(this, R.color.colorAccent));
        } else {
            this.nfcLogoImageView.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary));
        }
    }
}
