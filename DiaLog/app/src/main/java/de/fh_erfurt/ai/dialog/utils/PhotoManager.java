package de.fh_erfurt.ai.dialog.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import de.fh_erfurt.ai.dialog.R;
import id.zelory.compressor.Compressor;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;

/**
 * PhotoManager
 * Utility class to start the camera and create picture by the result of the camera.
 *
 * @author Alexander Zaak
 * @version 1.0
 * @since 18.05.17
 */
public class PhotoManager {

    private static final int CAMERA_REQUEST_CODE = 2211;
    private static final String PHOTO_DATA_URI = "data";
    private static final String PROFILE_PHOTO_NAME = "profile";
    private static Uri takenPhoto;

    /**
     * Method to start the phone camera and get the result in a fragment.
     * If permissions to take a photo are not granted, then the permissions will be requested.
     *
     * @param parentActivity Parent activity of the fragment
     * @param fragment       fragment that called the method
     * @throws IOException will be thrown, when directory cannot be created
     */
    public static void startCamera(Activity parentActivity, Fragment fragment) throws IOException {
        if (parentActivity == null || fragment == null) {
            return;
        }

        if (!canStartCamera(parentActivity)) {
            return;
        }

        Context context = fragment.getContext();
        Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takenPhoto = getPhotoUri(context);
        takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, takenPhoto);
        takePhotoIntent.putExtra(PHOTO_DATA_URI, takenPhoto.toString());
        fragment.startActivityForResult(takePhotoIntent, CAMERA_REQUEST_CODE);
    }

    /**
     * Method to set a byte array into an imageview
     *
     * @param ctx        Context
     * @param imageView  imageView
     * @param imageBytes byte array
     * @param photoName  photo name
     */
    public static void setPhoto(final Context ctx, final ImageView imageView, final byte[] imageBytes, final String photoName) {
        File photo = getPhoto(ctx, imageBytes, photoName);
        if (photo == null || photoName == null) {
            Picasso.with(ctx).load(R.drawable.default_profile_photo).into(imageView);
        } else {
            Picasso.with(ctx).load(photo).placeholder(R.drawable.default_profile_photo).into(imageView);
        }
    }

    /**
     * Method to get photo URI by indicating the request code and result code.
     *
     * @param requestCode request code
     * @param resultCode  result code
     * @return photo URI, when result or request code not matching the expected one then return NULL
     */
    public static Uri getPhoto(int requestCode, int resultCode) {
        if (takenPhoto != null && requestCode == CAMERA_REQUEST_CODE && resultCode == RESULT_OK) {
            return takenPhoto;
        }
        return null;
    }

    /**
     * Method to get byte array from uri
     *
     * @param ctx   Context
     * @param photo uri
     * @return byte array
     */
    public static byte[] getBytes(final Context ctx, Uri photo) {
        try {
            Bitmap bitmap = new Compressor(ctx).compressToBitmap(getFile(photo));
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            return stream.toByteArray();
        } catch (IOException e) {
            Timber.e(e);
        }
        return null;
    }

    /**
     * Method to get a compressed file from uri
     *
     * @param ctx   Context
     * @param photo uri
     * @return compressed file
     */
    public static File getCompressedFile(final Context ctx, Uri photo) {
        try {
            return new Compressor(ctx).compressToFile(Environment.getExternalStoragePublicDirectory(photo.getPath()));
        } catch (IOException e) {
            Timber.e(e);
            return Environment.getExternalStoragePublicDirectory(photo.getPath());
        }
    }

    /**
     * Method to get a file from uri
     *
     * @param photo uri
     * @return file
     */
    public static File getFile(Uri photo) {
        return Environment.getExternalStoragePublicDirectory(photo.getPath());
    }

    /**
     * Method to check permission for storage access to show a photo
     *
     * @param activity activity
     * @return true, if storage access is allowed, else false
     */
    public static boolean canShowPhoto(final Activity activity) {
        String[] permissionsToGrand = PermissionsCheck.checkRequiredPermissions(activity, PermissionsCheck.PERMISSION_SHOW_PHOTO);
        if (permissionsToGrand.length == 0) {
            return true;
        }
        PermissionsCheck.requestPermissions(activity, permissionsToGrand);
        return false;
    }

    /**
     * Method to get photo URI by indicating the picture path.
     *
     * @param picturePath path to the picture
     * @return if path is valid then return the photo URI, else return empty URI
     */
    static Uri getPhoto(String picturePath) {
        if (picturePath != null) {
            return Uri.parse(picturePath);
        }
        return Uri.EMPTY;
    }

    /**
     * Helper method to get a photo from byte array as a file
     *
     * @param ctx  Context
     * @param data byte code of the image
     * @param name file name
     * @return file
     */
    private static File getPhoto(final Context ctx, final byte[] data, final String name) {
        if (data != null) {
            try {
                File image = createPhoto(ctx, name);
                if (!image.exists()) {
                    FileOutputStream fos = new FileOutputStream(image.getPath());
                    fos.write(data);
                    fos.close();
                }
                return image;
            } catch (IOException e) {
                Timber.e(e);
            }
        }
        return null;
    }

    /**
     * Helper method to check permission for camera usage
     *
     * @param parentActivity activity
     * @return true, if camera usage is allowed, else false
     */
    private static boolean canStartCamera(Activity parentActivity) {
        String[] permissionsToGrand = PermissionsCheck.checkRequiredPermissions(parentActivity, PermissionsCheck.PERMISSION_TAKE_PHOTO);
        if (permissionsToGrand.length == 0) {
            return true;
        }
        PermissionsCheck.requestPermissions(parentActivity, permissionsToGrand);
        return false;
    }

    /**
     * Helper method to create a folder and a file for the photo.
     *
     * @param ctx Context
     * @return created File
     * @throws IOException IOException will be thrown, when directory cannot be created
     */
    private static File createPhoto(final Context ctx) throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.GERMAN).format(new Date());
        String name = "IMG_" + timeStamp + ".jpg";
        return createPhoto(ctx, name);
    }

    /**
     * Helper method to create a folder and a file for the photo.
     *
     * @param ctx  Context
     * @param name file name
     * @return created File
     * @throws IOException IOException will be thrown, when directory cannot be created
     */
    private static File createPhoto(final Context ctx, String name) throws IOException {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), ctx.getString(R.string.app_name));

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                throw new IOException("Directory cant be created");
            }
        }

        if (name == null) {
            name = PROFILE_PHOTO_NAME;
        }

        return new File(mediaStorageDir.getPath() + File.separator + name);
    }

    /**
     * Helper method to get the photo uri.
     *
     * @param ctx Context
     * @return Photo URI
     * @throws IOException IOException will be thrown, when directory cannot be created
     */
    private static Uri getPhotoUri(Context ctx) throws IOException {
        return FileProvider.getUriForFile(ctx, ctx.getPackageName() + ".de.fh_erfurt.ai.dialog", createPhoto(ctx));
    }
}
