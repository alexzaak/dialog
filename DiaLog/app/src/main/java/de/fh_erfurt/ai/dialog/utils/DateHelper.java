package de.fh_erfurt.ai.dialog.utils;

import org.joda.time.DateTime;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import timber.log.Timber;

/**
 * DateHelper
 * Utility class for date operations
 *
 * @author Alexander Zaak
 * @version 1.0
 * @since 31.05.17
 */
public class DateHelper {

    /**
     * Method sets hours, minutes, seconds and milliseconds to zero.
     *
     * @param date date that to be set to start
     * @return started day
     */
    public static Date dayStarted(Date date) {
        if (isNullValue(date)) {
            return null;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    /**
     * Method sets hours to 23 and minutes, seconds and milliseconds to 59
     *
     * @param date date taht to be set to end
     * @return ended day
     */
    public static Date dayEnded(Date date) {
        if (isNullValue(date)) {
            return null;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 59);
        return cal.getTime();
    }

    /**
     * Method to get the current part (morning, noon or evening) of the day.
     *
     * @return -1 = morning, 0 = noon, 1 = evening
     */
    public static int getDayTime() {
        Calendar cal = Calendar.getInstance();

        int hourOfDay = cal.get(Calendar.HOUR_OF_DAY);

        // Morning
        if (hourOfDay >= 0 && hourOfDay < 11) {
            return -1;
        }

        // Noon
        if (hourOfDay >= 11 && hourOfDay < 14) {
            return 0;
        }

        // Evening
        return 1;
    }

    /**
     * Method to check if the given dates are the same.
     *
     * @param date1 date to check
     * @param date2 date to check
     * @return true, if given dates the same, else false
     */
    public static boolean sameDay(Date date1, Date date2) {
        if (isNullValue(date1, date2)) {
            return false;
        }
        Date dayStarted = DateHelper.dayStarted(date2);
        Date dayEnded = DateHelper.dayEnded(date2);

        return !date1.before(dayStarted) && !date1.after(dayEnded);
    }

    /**
     * Method to formatting the given date.
     *
     * @param date date that to be formatted
     * @return formatted date
     */
    public static String getFormattedDate(Date date) {
        if (isNullValue(date)) {
            return null;
        }
        return DateFormat.getDateInstance().format(date);
    }

    /**
     * Method to get formatted date by indicating the date and the format.
     *
     * @param date   date that to be formatted
     * @param format the pattern describing the date and time format
     * @return the formatted date
     */
    public static String getFormattedDate(Date date, String format) {
        if (isNullValue(date, format) || format.isEmpty()) {
            return null;
        }

        SimpleDateFormat sdf;
        try {
            sdf = new SimpleDateFormat(format, Locale.getDefault());
        } catch (IllegalArgumentException e) {
            return null;
        }
        return sdf.format(date);
    }

    /**
     * Returns Date from given unix time stamp
     *
     * @param value unix time stamp
     * @return Date
     */
    public static Date fromUnixTime(long value) {
        return new Date(value * 1000L);
    }

    /**
     * Return unix time stamp from given Date
     *
     * @param dt date
     * @return unix time stamp
     */
    public static long toUnixTime(Date dt) {
        if (dt == null) {
            return 0L;
        }
        return dt.getTime();
    }

    /**
     * Returns the parsed Date from string value
     *
     * @param value value
     * @return Date
     */
    public static Date parse(String value) {
        if (value == null) {
            return null;
        }
        try {
            return DateTime.parse(value).toDate();
        } catch (IllegalArgumentException e) {
            Timber.e(e);
        }
        return null;
    }

    /**
     * Helper method to check null values
     *
     * @param values values to be checked
     * @return true, if one of the values are null, else false
     */
    private static boolean isNullValue(Object... values) {
        for (Object value : values) {
            if (value == null) {
                return true;
            }
        }
        return false;
    }
}
