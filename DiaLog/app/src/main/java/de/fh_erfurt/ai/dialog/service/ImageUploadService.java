package de.fh_erfurt.ai.dialog.service;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.concurrent.atomic.AtomicInteger;

import de.fh_erfurt.ai.dialog.R;
import de.fh_erfurt.ai.dialog.controller.App;
import de.fh_erfurt.ai.dialog.food.FoodApi;
import okhttp3.ResponseBody;
import timber.log.Timber;

/**
 * Service to upload an image as a background job
 *
 * @author Alexander Zaak
 * @version 1.0
 * @since 21.01.18
 */
public class ImageUploadService extends IntentService {


    private static final String EXTRA_FOOD_ID = "food_id";
    private static final String EXTRA_START_ID = "start_id";
    private static final String SERVICE_NAME = "image_upload_service";
    private static final int DEFAULT_START_ID = -1;

    private static final String UPLOAD_NOTIFICATION_CHANNEL_ID = "upload_id";

    private final AtomicInteger atomicId = new AtomicInteger();

    public ImageUploadService() {
        super(SERVICE_NAME);
    }

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public ImageUploadService(String name) {
        super(name);

    }

    @Override
    public void onStart(@Nullable Intent intent, int startId) {
        if (intent == null) {
            return;
        }
        startId = atomicId.getAndIncrement();
        intent.putExtra(EXTRA_START_ID, startId);
        super.onStart(intent, startId);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent == null) {
            return;
        }

        final Context ctx = getApplicationContext();

        final NotificationManager notificationManager = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
        final String foodId = intent.getStringExtra(EXTRA_FOOD_ID);
        final int startId = intent.getIntExtra(EXTRA_START_ID, DEFAULT_START_ID);

        final Uri imageUri = intent.getData();
        if (imageUri == null) {
            stopSelf(startId);
            return;
        }

        showNotification(startId, notificationManager);

        FoodApi.postImage(ctx, foodId, imageUri).addOnCompleteListener(new OnCompleteListener<ResponseBody>() {
            @Override
            public void onComplete(@NonNull Task<ResponseBody> task) {
                if (task.isSuccessful()) {
                    if (notificationManager != null) {
                        notificationManager.cancel(startId);
                        stopSelf(startId);
                    }
                }
            }
        });
    }

    private void showNotification(final int id, final NotificationManager notificationManager) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this, UPLOAD_NOTIFICATION_CHANNEL_ID)
                        .setContentTitle(this.getString(R.string.app_name))
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(this.getString(R.string.description_image_upload)))
                        .setContentText(this.getString(R.string.description_image_upload))
                        .setProgress(0, 0, true)
                        .setSmallIcon(R.mipmap.ic_launcher);

        notificationManager.notify(id, mBuilder.build());
    }

    /**
     * Method to start the service
     *
     * @param foodId   food id, which the image belongs
     * @param imageUri image uri
     */
    public static void startService(final String foodId, final Uri imageUri) {
        if (foodId == null || imageUri == null) {
            Timber.e("Context or foodId or imageUri was null");
            return;
        }
        final Context ctx = App.getContext();
        Intent intent = new Intent(Intent.ACTION_SYNC, imageUri, ctx, ImageUploadService.class);
        intent.putExtra(EXTRA_FOOD_ID, foodId);
        ctx.startService(intent);
    }
}
