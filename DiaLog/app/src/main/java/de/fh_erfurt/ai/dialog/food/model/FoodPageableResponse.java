package de.fh_erfurt.ai.dialog.food.model;

import java.util.List;

/**
 * Created by alexander on 07.01.18.
 */

public class FoodPageableResponse {
    private List<FoodResponse> elements;
    private int size;
    private int page;
    private int totalElements;
    private int totalPages;

    public List<FoodResponse> getElements() {
        return elements;
    }

    public void setElements(List<FoodResponse> elements) {
        this.elements = elements;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(int totalElements) {
        this.totalElements = totalElements;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    @Override
    public String toString() {
        return "FoodPageableResponse{" +
                "elements=" + elements.size() +
                ", size=" + size +
                ", page=" + page +
                ", totalElements=" + totalElements +
                ", totalPages=" + totalPages +
                '}';
    }
}
