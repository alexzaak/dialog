package de.fh_erfurt.ai.dialog.utils;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

/**
 * PermissionsCheck
 * Utility class to check permissions and request required permissions.
 *
 * @author Alexander Zaak
 * @version 1.0
 * @since 21.05.17
 */
class PermissionsCheck {

    static final String[] PERMISSION_TAKE_PHOTO = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA};

    static final String[] PERMISSION_SHOW_PHOTO = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE};

    private static final boolean IS_ANDROID_M_OR_HIGHER = android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M;
    private static final int PERMISSIONS_REQUEST_CODE = 3101;

    /**
     * Method to check required permissions
     *
     * @param activity            activity that request the permission
     * @param requiredPermissions permissions that are required
     * @return permissions that are not granted. if all request permissions granted then the array is empty
     */
    @TargetApi(Build.VERSION_CODES.M)
    static String[] checkRequiredPermissions(Activity activity, String[] requiredPermissions) {
        List<String> permissionsToRequest = new ArrayList<>();

        for (String permission : requiredPermissions) {
            if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsToRequest.add(permission);
            }
        }
        return permissionsToRequest.toArray(new String[permissionsToRequest.size()]);
    }

    /**
     * Method to request permissions.
     *
     * @param activity    that request the permission
     * @param permissions permissions to be requested
     */
    @TargetApi(Build.VERSION_CODES.M)
    static void requestPermissions(Activity activity, String[] permissions) {
        if (IS_ANDROID_M_OR_HIGHER && permissions.length > 0) {
            activity.requestPermissions(permissions, PERMISSIONS_REQUEST_CODE);
        }
    }
}
