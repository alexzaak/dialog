package de.fh_erfurt.ai.dialog.utils;

import android.widget.EditText;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

/**
 * Utility class for double, string and format handling.
 *
 * @author Alexander Zaak
 * @version 1.0
 * @since 11.02.18
 */
public class NumberHelper {

    private final DecimalFormat df;

    public NumberHelper() {
        DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance();
        dfs.setDecimalSeparator('.');
        this.df = new DecimalFormat("##.#", dfs);
    }

    /**
     * Method to format double value (default format: ##.#)
     *
     * @param value value to format
     * @return formatted value
     */
    public String format(final double value) {
        return this.df.format(value);
    }

    /**
     * Returns the parsed double value of an input instance. In case of an unparsable
     * value 0 will be returned.
     *
     * @param input - The input instance.
     * @return The parsed double value or 0;
     */
    public static double getDoubleValue(EditText input) {
        String text = input.getText().toString();

        if (text.equals("")) {
            return 0;
        }

        try {
            return Double.parseDouble(text);
        } catch (NumberFormatException ex) {
            return 0;
        }
    }

    /**
     * Returns the parsed string value of a double value. In case of a null
     * value, an empty string will be returned.
     *
     * @param value - The double value.
     * @return The parsed String value or empty string;
     */
    public static String toString(Double value) {
        if (value == null) {
            return "";
        }
        return Double.toString(value);
    }
}
