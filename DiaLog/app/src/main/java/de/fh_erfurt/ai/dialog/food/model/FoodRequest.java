package de.fh_erfurt.ai.dialog.food.model;

/**
 * Created by alexander on 10.01.18.
 */

public class FoodRequest {
    private String name;
    private double carbs;

    public FoodRequest(String name, double carbs) {
        this.name = name;
        this.carbs = carbs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCarbs() {
        return carbs;
    }

    public void setCarbs(double carbs) {
        this.carbs = carbs;
    }
}
