package de.fh_erfurt.ai.dialog.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Pair;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.sectionedrecyclerview.SectionedRecyclerViewAdapter;
import com.afollestad.sectionedrecyclerview.SectionedViewHolder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.fh_erfurt.ai.dialog.CreateLogEntryActivity;
import de.fh_erfurt.ai.dialog.R;
import de.fh_erfurt.ai.dialog.controller.LogManager;
import de.fh_erfurt.ai.dialog.controller.UserSettingsManager;
import de.fh_erfurt.ai.dialog.enums.BloodsugarUnit;
import de.fh_erfurt.ai.dialog.enums.CarbsUnit;
import de.fh_erfurt.ai.dialog.enums.Column;
import de.fh_erfurt.ai.dialog.model.LogEntry;
import de.fh_erfurt.ai.dialog.model.User;
import de.fh_erfurt.ai.dialog.utils.DateHelper;
import io.realm.ObjectChangeSet;
import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.RealmObjectChangeListener;
import io.realm.RealmResults;
import io.realm.Sort;
import mobi.upod.timedurationpicker.TimeDurationUtil;

/**
 * Adapter for sectioned recycler view. Used for log view.
 *
 * @author Max Edenharter
 */
public class LogAdapter extends SectionedRecyclerViewAdapter<SectionedViewHolder> {
    private List<Pair<Date, ArrayList<LogEntry>>> sectionedLog;
    private RealmResults<LogEntry> logResults;
    private User user;
    private Context ctx;

    public LogAdapter(Context ctx) {
        this.ctx = ctx;
        this.user = UserSettingsManager.getManagedUser();

        this.logResults = LogManager.getAllLogEntries();
        this.sectionedLog = new ArrayList<>();
        this.buildSections(this.logResults);
        collapseAllSections();

        this.user.addChangeListener(new RealmObjectChangeListener<User>() {
            @Override
            public void onChange(User changedUser, ObjectChangeSet changeSet) {
                user = changedUser;
                buildSections(logResults);
                notifyDataSetChanged();
            }
        });

        this.logResults.addChangeListener(new OrderedRealmCollectionChangeListener<RealmResults<LogEntry>>() {
            @Override
            public void onChange(RealmResults<LogEntry> collection, OrderedCollectionChangeSet changeSet) {
                buildSections(collection);
                notifyDataSetChanged();
            }
        });
    }

    /**
     * @param parent   The parent view group of the current view holder.
     * @param viewType The current view type.
     * @return The right view holder based on current view type (header or child entrx)
     */
    @Override
    public SectionedViewHolder onCreateViewHolder(ViewGroup parent,
                                                  int viewType) {
        View v;

        if (viewType == VIEW_TYPE_HEADER) {
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.card_log_date, parent, false);
            return new HeaderViewHolder(v);
        } else {
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.card_log_entry, parent, false);
            return new EntryViewHolder(v);
        }
    }

    @Override
    public int getSectionCount() {
        return this.sectionedLog.size();
    }

    @Override
    public int getItemCount(int section) {
        return this.sectionedLog.get(section).second.size();
    }

    /**
     * Fills header view holder with content.
     *
     * @param holder   The current view holder.
     * @param section  The current section index.
     * @param expanded Is the current section expanded or not.
     */
    @Override
    public void onBindHeaderViewHolder(SectionedViewHolder holder, final int section, boolean expanded) {
        HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
        Date sectionDate = this.sectionedLog.get(section).first;
        headerViewHolder.dateView.setText(DateHelper.getFormattedDate(sectionDate, "EEEE, dd.MM.yyyy"));

        double bloodSugarAverage = LogManager.getAveragedValue(Column.BLOODSUGAR, DateHelper.dayStarted(sectionDate), DateHelper.dayEnded(sectionDate));
        String bloodSugarUnit = ctx.getString(BloodsugarUnit.getUnit(this.user.getBloodSugarUnit()).getNameRes());
        headerViewHolder.averageView.setText(ctx.getString(R.string.label_bloodsugar_average, bloodSugarAverage, bloodSugarUnit));

        headerViewHolder.caretView.setImageResource(expanded ? R.drawable.ic_collapse : R.drawable.ic_expand);
        headerViewHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleSectionExpanded(section);
            }
        });
    }

    /**
     * Fills the footer view holder with content.
     *
     * @param holder  The current view holder.
     * @param section The current section.
     */
    @Override
    public void onBindFooterViewHolder(SectionedViewHolder holder, int section) {
    }

    /**
     * Fills the entry view holder with content.
     *
     * @param holder           The current view holder.
     * @param section          The current section index.
     * @param relativePosition Index in current section.
     * @param absolutePosition The current overall index, including headers and footers.
     */
    @Override
    public void onBindViewHolder(SectionedViewHolder holder, int section, int relativePosition, int absolutePosition) {
        EntryViewHolder entryViewHolder = (EntryViewHolder) holder;
        final LogEntry entry = this.sectionedLog.get(section).second.get(relativePosition);

        entryViewHolder.colorIndicatorView.setBackgroundColor(this.getColorByBloodsugar(entry.getBloodSugar()));
        entryViewHolder.timeView.setText(DateHelper.getFormattedDate(entry.getCreatedAt(), "HH:mm"));
        entryViewHolder.bloodSugarView.setText(ctx.getString(R.string.label_average_value, entry.getBloodSugar()));
        entryViewHolder.bloodSugarUnitView.setText(BloodsugarUnit.getUnit(user.getBloodSugarUnit()).getNameRes());
        entryViewHolder.bolusView.setText(ctx.getString(R.string.label_average_value, entry.getBolus() + entry.getCorrection()));
        entryViewHolder.carbsView.setText(ctx.getString(R.string.label_average_value, entry.getCarbs()));
        entryViewHolder.carbsUnitView.setText(CarbsUnit.getUnit(user.getCarbsUnit()).getNameRes());
        entryViewHolder.activityView.setText(String.format("%02d:%02d", TimeDurationUtil.hoursOf(entry.getActivity()), TimeDurationUtil.minutesInHourOf(entry.getActivity())));

        entryViewHolder.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                menu.setHeaderTitle(DateHelper.getFormattedDate(entry.getCreatedAt(), "dd.MM.yyyy HH:mm"));
                menu.add(Menu.NONE, v.getId(), 0, R.string.label_edit).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        ctx.startActivity(CreateLogEntryActivity.newIntent(ctx, entry.getId()));
                        return false;
                    }
                });
                menu.add(Menu.NONE, v.getId(), 1, R.string.label_delete).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        LogManager.deleteLogEntry(entry);
                        return false;
                    }
                });
            }
        });
    }

    /**
     * Builds sections from realm results of log entries. On section symbolizes a day in log.
     *
     * @param log The log entries from realm.
     */
    private void buildSections(RealmResults<LogEntry> log) {
        if (log.isEmpty()) {
            this.sectionedLog.clear();
            return;
        }

        log = log.sort(Column.CREATED_AT.getName(), Sort.DESCENDING);
        Date nextDay = log.get(0).getCreatedAt();
        Pair<Date, ArrayList<LogEntry>> section = new Pair<>(nextDay, new ArrayList<LogEntry>());
        this.sectionedLog.clear();

        for (LogEntry entry : log) {
            Date date = entry.getCreatedAt();

            if (!DateHelper.sameDay(date, nextDay)) {
                this.sectionedLog.add(section);

                section = new Pair<>(date, new ArrayList<LogEntry>());
                nextDay = date;
            }

            section.second.add(entry);
        }

        if (!section.second.isEmpty()) {
            this.sectionedLog.add(section);
        }
    }

    /**
     * Calculates the color of log entries by their bloodsugar value.
     *
     * @param bloodSugar The bloodsugar value of the log entry.
     * @return A color value.
     */
    private int getColorByBloodsugar(double bloodSugar) {
        float[] hsv = {120, 1, 0.95f};

        // Bloodsugar values in target area are green (HSV color value 120).
        if (bloodSugar >= this.user.getTargetAreaFrom() && bloodSugar <= this.user.getTargetAreaTo()) {
            return Color.HSVToColor(hsv);
        }

        double hyperTreshold = this.user.getThresholdHyper();
        double hypoTreshold = this.user.getThresholdHypo();

        if (bloodSugar > hyperTreshold) {
            // In case of a hyper the color is between blue and magenta.
            float colorValue = (float) (210 * (bloodSugar / hyperTreshold));
            hsv[0] = colorValue <= 300 ? colorValue : 300;
        } else if (bloodSugar < hypoTreshold) {
            // In case of a hypo the color is between orange and red.
            hsv[0] = (float) (30 * (bloodSugar / hypoTreshold));
        } else {
            // Values outside the target area and which are not a hyper or hypo are yellow (HSV color value 60).
            hsv[0] = 60;
        }

        return Color.HSVToColor(hsv);
    }

    public static class HeaderViewHolder extends SectionedViewHolder {
        @BindView(R.id.log_entry_date)
        TextView dateView;

        @BindView(R.id.log_entry_caret)
        ImageView caretView;

        @BindView(R.id.log_entry_average)
        TextView averageView;

        private View v;

        HeaderViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
            this.v = v;
        }

        public void setOnClickListener(View.OnClickListener listener) {
            v.setOnClickListener(listener);
        }
    }

    public static class EntryViewHolder extends SectionedViewHolder {
        private View v;

        @BindView(R.id.log_entry_color_indicator)
        View colorIndicatorView;
        @BindView(R.id.log_entry_time)
        TextView timeView;
        @BindView(R.id.log_entry_blood_sugar)
        TextView bloodSugarView;
        @BindView(R.id.log_entry_blood_sugar_unit)
        TextView bloodSugarUnitView;
        @BindView(R.id.log_entry_carbs)
        TextView carbsView;
        @BindView(R.id.log_entry_carbs_unit)
        TextView carbsUnitView;
        @BindView(R.id.log_entry_bolus)
        TextView bolusView;
        @BindView(R.id.log_entry_activity)
        TextView activityView;


        EntryViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
            this.v = v;
        }

        void setOnCreateContextMenuListener(View.OnCreateContextMenuListener listener) {
            v.setOnCreateContextMenuListener(listener);
        }
    }
}


