package de.fh_erfurt.ai.dialog.food.model;

import com.squareup.moshi.Json;

import java.util.Date;

/**
 * Created by alexander on 06.12.17.
 */

public class FoodResponse {
    @Json(name = "_id")
    private String id;
    private String name;
    private double carbs;
    private String image;
    private Date createdAt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCarbs() {
        return carbs;
    }

    public void setCarbs(double carbs) {
        this.carbs = carbs;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
