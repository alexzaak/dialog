package de.fh_erfurt.ai.dialog.utils;

import de.fh_erfurt.ai.dialog.model.User;

/**
 * Bolus calculator
 * Calculates bolus and correction recommendations based on the current bloodsugar
 * and amount of carbs to eat.
 *
 * @author Max Edenharter
 */
public class BolusCalc {
    private User user;

    public BolusCalc(User user) {
        this.user = user;
    }

    /**
     * Calculates a bolus recommendation by the patients bloodsugar value,
     * the day time and the carbs to eat.
     *
     * @param bloodSugar The current bloodsugar value.
     * @param carbs      Amount of carbs to eat.
     * @return The calculated bolus recommendation.
     */
    public double calcBolus(double bloodSugar, double carbs) {

        if (bloodSugar < user.getTargetAreaFrom()) {
            return 0;
        }

        switch (DateHelper.getDayTime()) {
            case -1:
                return carbs * user.getBolusFactorMorning();
            case 0:
                return carbs * user.getBolusFactorNoon();
            default:
                return carbs * user.getBolusFactorEvening();
        }
    }

    /**
     * Calculates a correction recommendation by patients bloodsugar value.
     *
     * @param bloodSugar The current bloodsugar value.
     * @return The calculated correction recommendation.
     */
    public double calcCorrection(double bloodSugar) {
        if (bloodSugar <= user.getTargetAreaTo() || user.getBolusFactorCorrection() <= 0) {
            return 0;
        }

        double target = (user.getTargetAreaTo() + user.getTargetAreaFrom()) / 2;

        return (bloodSugar - target) / user.getBolusFactorCorrection();
    }
}
