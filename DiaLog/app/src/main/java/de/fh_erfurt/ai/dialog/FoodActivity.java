package de.fh_erfurt.ai.dialog;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import de.fh_erfurt.ai.dialog.fragment.AddFoodFragment;
import de.fh_erfurt.ai.dialog.fragment.SearchFoodFragment;
import timber.log.Timber;

/**
 * FoodActivity for food search and add food
 *
 * @author Alexander Zaak
 * @version 1.0
 * @since 10.01.18
 */
public class FoodActivity extends AppCompatActivity implements SearchFoodFragment.OnInteractionListener, AddFoodFragment.OnInteractionListener {

    public static final String EXTRA_CARBS = "extra_carbs";

    private static final String FOOD_ADD_FRAGMENT_TAG = "food_add_fragment_tag";
    private static final String FOOD_SEARCH_FRAGMENT_TAG = "food_search_fragment_tag";

    private static final int FOOD_SEARCH_FRAGMENT = 0;
    private static final int FOOD_ADD_FRAGMENT = 1;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food);

        // Check that the activity is using the layout version with
        // the fragment_container FrameLayout
        if (findViewById(R.id.content) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

            this.setFragment(FOOD_SEARCH_FRAGMENT);
        }
    }

    @Override
    public void onAddBtnPressed(String searchKey) {
        this.setFragment(FOOD_ADD_FRAGMENT, searchKey);
    }

    @Override
    public void onFoodSelected(final double carbs) {
        this.newIntent(carbs);
    }

    @Override
    public void onFoodAdded(final double carbs) {
        this.newIntent(carbs);
    }

    @Override
    public void onCancel() {
        onBackPressed();
    }

    private void setFragment(final int id) {
        this.setFragment(id, null);
    }

    private void setFragment(final int id, final String searchKey) {

        switch (id) {
            case FOOD_SEARCH_FRAGMENT:
                getSupportFragmentManager().beginTransaction().add(R.id.content, new SearchFoodFragment(), FOOD_SEARCH_FRAGMENT_TAG).commit();
                break;

            case FOOD_ADD_FRAGMENT:
                getSupportFragmentManager().beginTransaction().add(R.id.content, AddFoodFragment.create(searchKey), FOOD_ADD_FRAGMENT_TAG).addToBackStack(null).commit();
                break;
            default:
                Timber.e("fragment is null");
        }
    }

    private void newIntent(final double carbs) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(EXTRA_CARBS, carbs);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    public static Intent newInstance(Context ctx) {
        return new Intent(ctx, FoodActivity.class);
    }
}
