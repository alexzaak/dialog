package de.fh_erfurt.ai.dialog.utils;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * Utility class to handle view specific behaviour.
 *
 * @author Alexander Zaak
 * @version 1.0
 * @since 31.01.18
 */
public class ViewUtils {

    public static void hideKeyboard(final Context ctx, final View view) {
        if (view == null || ctx == null) {
            return;
        }
        InputMethodManager imm = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
