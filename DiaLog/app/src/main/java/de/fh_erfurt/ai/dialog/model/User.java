package de.fh_erfurt.ai.dialog.model;

import java.util.UUID;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Model class for user settings.
 *
 * @author Max Edenharter
 */
public class User extends RealmObject {
    @PrimaryKey
    private String id = UUID.randomUUID().toString();
    private String name;
    private byte[] profileImage;
    private String photoName;
    private int bloodSugarUnit;
    private int carbsUnit;
    private double bolusFactorMorning;
    private double bolusFactorNoon;
    private double bolusFactorEvening;
    private double bolusFactorCorrection;
    private double thresholdHyper;
    private double thresholdHypo;
    private double targetAreaFrom;
    private double targetAreaTo;

    private RealmList<LogEntry> logEntryList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(byte[] profileImage) {
        this.profileImage = profileImage;
    }

    public String getPhotoName() {
        return photoName;
    }

    public void setPhotoName(String photoName) {
        this.photoName = photoName;
    }

    public int getBloodSugarUnit() {
        return bloodSugarUnit;
    }

    public void setBloodSugarUnit(int bloodSugarUnit) {
        this.bloodSugarUnit = bloodSugarUnit;
    }

    public int getCarbsUnit() {
        return carbsUnit;
    }

    public void setCarbsUnit(int carbsUnit) {
        this.carbsUnit = carbsUnit;
    }

    public double getBolusFactorMorning() {
        return bolusFactorMorning;
    }

    public void setBolusFactorMorning(double bolusFactorMorning) {
        this.bolusFactorMorning = bolusFactorMorning;
    }

    public double getBolusFactorNoon() {
        return bolusFactorNoon;
    }

    public void setBolusFactorNoon(double bolusFactorNoon) {
        this.bolusFactorNoon = bolusFactorNoon;
    }

    public double getBolusFactorEvening() {
        return bolusFactorEvening;
    }

    public void setBolusFactorEvening(double bolusFactorEvening) {
        this.bolusFactorEvening = bolusFactorEvening;
    }

    public double getBolusFactorCorrection() {
        return bolusFactorCorrection;
    }

    public void setBolusFactorCorrection(double bolusFactorCorrection) {
        this.bolusFactorCorrection = bolusFactorCorrection;
    }

    public RealmList<LogEntry> getLogEntryList() {
        return logEntryList;
    }

    public void setLogEntryList(RealmList<LogEntry> logEntryList) {
        this.logEntryList = logEntryList;
    }

    public double getThresholdHyper() {
        return thresholdHyper;
    }

    public void setThresholdHyper(double thresholdHyper) {
        this.thresholdHyper = thresholdHyper;
    }

    public double getThresholdHypo() {
        return thresholdHypo;
    }

    public void setThresholdHypo(double thresholdHypo) {
        this.thresholdHypo = thresholdHypo;
    }

    public double getTargetAreaFrom() {
        return targetAreaFrom;
    }

    public void setTargetAreaFrom(double targetAreaFrom) {
        this.targetAreaFrom = targetAreaFrom;
    }

    public double getTargetAreaTo() {
        return targetAreaTo;
    }

    public void setTargetAreaTo(double targetAreaTo) {
        this.targetAreaTo = targetAreaTo;
    }
}
