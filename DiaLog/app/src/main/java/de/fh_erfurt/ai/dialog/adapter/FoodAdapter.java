package de.fh_erfurt.ai.dialog.adapter;

import android.content.Context;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.fh_erfurt.ai.dialog.BuildConfig;
import de.fh_erfurt.ai.dialog.R;
import de.fh_erfurt.ai.dialog.controller.UserSettingsManager;
import de.fh_erfurt.ai.dialog.enums.CarbsUnit;
import de.fh_erfurt.ai.dialog.food.model.FoodPageableResponse;
import de.fh_erfurt.ai.dialog.food.model.FoodResponse;
import de.fh_erfurt.ai.dialog.model.User;
import de.fh_erfurt.ai.dialog.utils.DiaConverter;

/**
 * Adapter for the food list
 *
 * @author Alexander Zaak
 * @version 1.0
 * @since 10.01.18
 */
public class FoodAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public interface OnInteractionListener {
        void onMoreBtnClick(int nextPage);

        void onItemClick(FoodResponse food);
    }

    private static final int FOOTER_VIEW = 1;
    private static final int EMPTY_VIEW = 2;

    private final Context ctx;
    private final List<FoodResponse> foodList = new ArrayList<>();
    private final User userSettings;

    private final LayoutInflater inflater;
    private final OnInteractionListener listener;

    private int currentPage = 0;
    private int totalPages = 0;

    public FoodAdapter(final Context ctx, final List<FoodResponse> foodList, OnInteractionListener listener) {
        this.ctx = ctx;
        this.foodList.addAll(foodList);
        this.userSettings = UserSettingsManager.getUser();
        this.inflater = LayoutInflater.from(ctx);
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == EMPTY_VIEW) {
            return new EmptyViewHolder(inflater.inflate(R.layout.food_empty_row, parent, false));
        }
        if (viewType == FOOTER_VIEW) {
            return new FooterViewHolder(inflater.inflate(R.layout.food_load_more_row, parent, false));
        }

        return new ItemViewHolder(inflater.inflate(R.layout.food_entry_row, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (this.foodList.isEmpty()) {
            return;
        }

        if (holder instanceof FooterViewHolder) {
            final FooterViewHolder vh = (FooterViewHolder) holder;

            vh.loadMoreBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final int nextPage = currentPage + 1;
                    listener.onMoreBtnClick(nextPage);
                }
            });
            return;
        }

        final FoodResponse food = this.foodList.get(position);
        ItemViewHolder vh = (ItemViewHolder) holder;

        Picasso.with(this.ctx)
                .load(String.format(BuildConfig.FOOD_API_IMAGE, food.getId()))
                .placeholder(R.drawable.ic_photo_default)
                .into(vh.photo);

        vh.foodName.setText(food.getName());

        final CarbsUnit currentCarbsUnit = CarbsUnit.getUnit(this.userSettings.getCarbsUnit());
        final double convertedResult = DiaConverter.convertCarbs(CarbsUnit.CU, currentCarbsUnit, food.getCarbs());
        vh.foodCarbs.setText(this.ctx.getString(R.string.format_two_digits, convertedResult));
        vh.carbsUnit.setText(currentCarbsUnit.getNameRes());

        vh.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(food);
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        if (this.foodList.isEmpty()) {
            return EMPTY_VIEW;
        }
        if (position == this.foodList.size()) {
            return FOOTER_VIEW;
        }

        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        if (this.foodList == null) {
            return 0;
        }
        // Add extra view to show the footer view
        return this.foodList.size() + 1;
    }


    public void add(FoodPageableResponse result) {
        this.currentPage = result.getPage();
        this.totalPages = result.getTotalPages();
        update(result.getElements());
    }

    private void update(final List<FoodResponse> items) {
        final DiffUtil.DiffResult diff = DiffUtil.calculateDiff(new DiffUtil.Callback() {
            @Override
            public int getOldListSize() {
                return getItemCount() - 1;
            }

            @Override
            public int getNewListSize() {
                return items.size();
            }

            @Override
            public boolean areItemsTheSame(final int oldItemPosition, final int newItemPosition) {
                final FoodResponse curr = foodList.get(oldItemPosition);
                final FoodResponse next = items.get(newItemPosition);

                return curr.getId().equals(next.getId());
            }

            @Override
            public boolean areContentsTheSame(final int oldItemPosition, final int newItemPosition) {
                return areItemsTheSame(oldItemPosition, newItemPosition);
            }
        });

        if (this.currentPage == 0) {
            this.foodList.clear();
        }
        this.foodList.addAll(items);

        diff.dispatchUpdatesTo(this);
    }

    class EmptyViewHolder extends RecyclerView.ViewHolder {
        EmptyViewHolder(View view) {
            super(view);
        }
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageview_food_photo)
        ImageView photo;

        @BindView(R.id.textview_food_name)
        TextView foodName;

        @BindView(R.id.textview_food_carbs)
        TextView foodCarbs;

        @BindView(R.id.textview_carbs_unit)
        TextView carbsUnit;

        ItemViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    class FooterViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.more)
        Button loadMoreBtn;

        FooterViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
