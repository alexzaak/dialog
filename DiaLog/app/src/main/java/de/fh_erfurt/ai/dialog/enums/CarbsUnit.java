package de.fh_erfurt.ai.dialog.enums;

import android.support.annotation.IdRes;
import android.support.annotation.StringRes;

import de.fh_erfurt.ai.dialog.R;

/**
 * Enumeration for carbs unit
 *
 * @author Max Edenharter
 * @version 1.0
 * @since 10.05.17
 */
public enum CarbsUnit {

    /* Unit Carbohydrate */
    CU(1, R.id.radio_carbs_unit_cu, R.string.label_carbs_unit_cu),
    /* Unit bread */
    BU(2, R.id.radio_carbs_unit_bu, R.string.label_carbs_unit_bu);

    private int id;
    private int radioRes;
    private int nameRes;

    CarbsUnit(int id, @IdRes int radioRes, @StringRes int nameRes) {
        this.id = id;
        this.radioRes = radioRes;
        this.nameRes = nameRes;
    }

    /**
     * Method to get Unit ID
     *
     * @return unit ID
     */
    public int getId() {
        return this.id;
    }

    /**
     * Method to get the corresponding radio button
     *
     * @return radio button ID {@link R.id}
     */
    public int getRadioRes() {
        return this.radioRes;
    }

    /**
     * Method to get Unit Name
     *
     * @return String resource {@link R.string}
     */
    public int getNameRes() {
        return this.nameRes;
    }

    /**
     * Method to get Bcarbs unit by given ID
     *
     * @param unit unit ID
     * @return {@link CarbsUnit}
     */
    public static CarbsUnit getUnit(int unit) {
        switch (unit) {
            case 1:
                return CU;
            case 2:
                return BU;
            default:
                return CU;
        }
    }

    /**
     * Method to Unit ID by given Radio Button ID
     *
     * @param checkedRadioButtonId Radio Button ID {@link R.id}
     * @return Unit ID
     */
    public static int getUnitId(@IdRes int checkedRadioButtonId) {
        switch (checkedRadioButtonId) {
            case R.id.radio_carbs_unit_cu:
                return CU.getId();
            case R.id.radio_carbs_unit_bu:
                return BU.getId();
            default:
                return CU.getId();
        }
    }
}
