package de.fh_erfurt.ai.dialog.utils;

import android.support.v4.content.FileProvider;

/**
 * CustomFileProvider
 * Provider is necessary to create folders and files in Android Nougat and higher. A custom FileProvider will be used in the AndroidManifest to register a FileProvider and accessible directories.
 *
 * @author Alexander Zaak
 * @version 1.0
 * @since 20.07.17
 */
public class CustomFileProvider extends FileProvider {
    // Custom FileProvider necessary for take photos in Android Nougat and higher
}
