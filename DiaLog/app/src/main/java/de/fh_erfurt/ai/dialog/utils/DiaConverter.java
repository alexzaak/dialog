package de.fh_erfurt.ai.dialog.utils;

import java.util.UnknownFormatConversionException;

import de.fh_erfurt.ai.dialog.enums.BloodsugarUnit;
import de.fh_erfurt.ai.dialog.enums.CarbsUnit;

/**
 * Converts diabetes specific value from one unit to another.
 *
 * @author Max Edenharter
 */
public class DiaConverter {

    private static final double MMOL_L_MG_DL_FACTOR = 18.0182;
    private static final double BU_CU_FACTOR = 1.2;

    /**
     * Converts a bloodsugar value from the given source unit to the given target unit.
     *
     * @param from  The source bloodsugar unit.
     * @param to    The target bloodsugar unit.
     * @param value The bloodsugar value to convert.
     * @return The converted value.
     * @throws UnknownFormatConversionException In case of a unpossible conversion an exception will be thrown.
     */
    public static double convertBloodSugar(BloodsugarUnit from, BloodsugarUnit to, double value) {
        if (from == to) {
            return value;
        }

        if (from == BloodsugarUnit.MMOL_L && to == BloodsugarUnit.MG_DL) {
            return value * MMOL_L_MG_DL_FACTOR;
        }

        if (from == BloodsugarUnit.MG_DL && to == BloodsugarUnit.MMOL_L) {
            return value / MMOL_L_MG_DL_FACTOR;
        }

        throw new UnknownFormatConversionException(String.format("Can not convert %s to %s!", from, to));
    }

    /**
     * Converts a carbs value from the given source unit to the given target unit.
     *
     * @param from  The source carbs unit.
     * @param to    The target carbs unit.
     * @param value The carbs value to convert.
     * @return The converted value.
     * @throws UnknownFormatConversionException In case of a unpossible conversion an exception will be thrown.
     */
    public static double convertCarbs(CarbsUnit from, CarbsUnit to, double value) {
        if (from == to) {
            return value;
        }

        if (from == CarbsUnit.BU && to == CarbsUnit.CU) {
            return value * BU_CU_FACTOR;
        }

        if (from == CarbsUnit.CU && to == CarbsUnit.BU) {
            return value / BU_CU_FACTOR;
        }

        throw new UnknownFormatConversionException(String.format("Can not convert %s to %s!", from, to));
    }
}
